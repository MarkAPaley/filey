program filey;

{$mode objfpc}{$H+}

uses
  //cmem, // c memory manager, supposed to cleam memory better
  {$IFDEF UNIX}{$IFDEF UseCThreads}
  cthreads,
  {$ENDIF}{$ENDIF}
  SysUtils, Interfaces, Forms, memdslaz, fileyfrm;

{$R *.res}

procedure MainProc;
begin
  {$if declared(useHeapTrace)}
    //setHeapTraceOutput('heap.trc');
 {$endIf}
  RequireDerivedFormResource := True;
  Application.Initialize;
  Application.CreateForm(TForm1, Form1);
  Application.Run;
end;

begin
  MainProc;
end.

