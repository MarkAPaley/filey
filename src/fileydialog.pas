unit fileydialog;

{$mode objfpc}{$H+}

interface

uses
  Buttons, Classes, Controls, Dialogs, ExtCtrls, Forms, Graphics,
  StdCtrls, SysUtils;

type

  { TDialogForm }

  TDialogForm = class(TForm)
    BitBtnOK: TBitBtn;
    DialogMemo: TMemo;
    Panel1: TPanel;
    Panel2: TPanel;
    Panel3: TPanel;
    procedure BitBtnOKClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var CloseAction: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormShow(Sender: TObject);
  end;

var
  DialogForm: TDialogForm;

implementation

{$R *.lfm}

procedure TDialogForm.BitBtnOKClick(Sender: TObject);
begin
  Close;
end;

procedure TDialogForm.FormClose(Sender: TObject; var CloseAction: TCloseAction);
begin
  CloseAction := caFree;
end;

procedure TDialogForm.FormCreate(Sender: TObject);
begin
  BitBtnOK.Kind := bkOK;
end;

procedure TDialogForm.FormResize(Sender: TObject);
begin
  BitBtnOK.Left := (Panel2.Width - BitBtnOK.Width) div 2;
  BitBtnOK.Top := (Panel2.Height - BitBtnOK.Height) div 2;
end;

{ TDialogForm }

procedure TDialogForm.FormShow(Sender: TObject);
begin
  BitBtnOK.Left := (Panel2.Width - BitBtnOK.Width) div 2;
  BitBtnOK.Top := (Panel2.Height - BitBtnOK.Height) div 2;
end;

end.
