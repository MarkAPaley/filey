{ Filey - Rename TV episode files consistently

  Copyright (C) 2023 Mark A Paley Software

  This source is free software; you can redistribute it and/or modify it under
  the terms of the GNU General Public License as published by the Free
  Software Foundation; either version 2 of the License, or (at your option)
  any later version.

  This code is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
  details.

  A copy of the GNU General Public License is available on the World Wide Web
  at <http://www.gnu.org/copyleft/gpl.html>. You can also obtain it by writing
  to the Free Software Foundation, Inc., 51 Franklin Street - Fifth Floor,
  Boston, MA 02110-1335, USA.
}
//Lazarus version 2.3.0

unit fileyfrm;

{$mode objfpc}
{$COPERATORS ON}
{$LONGSTRINGS ON}
{$SMARTLINK ON}
interface

uses
  {$IFDEF WINDOWS}ActiveX, ComObj, ShlObj, Win32Proc, Windows, {$ENDIF}
  ActnList, Buttons, Classes, Clipbrd, ComCtrls, Controls, CRT, csvdocument,
  DateUtils, DB, DBCtrls, dbf, DBGrids, Dialogs, Dos, EditBtn, ExtCtrls,
  FileInfo, FileUtil, fileydialog, fileyprogress, Forms, fpcunit, fpDBExport,
  fphttpclient, Graphics, Grids, GuiTestRunner, IniFiles, Interfaces,
  Ipfilebroker, IpHtml, Iphttpbroker, LazFileUtils, LazUTF8, lclintf, LCLProc,
  LCLType, LConvEncoding, LResources, Math, Menus, opensslsockets, process,
  RegExpr, StdCtrls, StrUtils, SynEdit, SynEditTypes, SynHighlighterPas,
  SynEditMouseCmds, SynGutterCodeFolding, SysUtils, testregistry, Types, TypInfo,
  Variants, Zipper, System.UITypes;

type
  { TMyGuiTestRunner }
  TMyGuiTestRunner = class(TGuiTestRunner)
    procedure FormClose(Sender: TObject; var Closeaction: Tcloseaction);
  end;

  THackDBNavigator = class(TDBNavigator)
  end;

  THackDBDataLink = class(TDBNavDataLink)
  end;

  TMyGrid = class(TCustomDBGrid)
  end;

  DBRecord = record
    aDB: TDbf;
    aDataSource: TDataSource;
    aFilterDescription: String;
    aFilterText: String;
    aTabIndex: Integer;
  end;

  //enumerating makes it easy to add/remove DBFs to the program
  DBFs = (ABOUT, EPS, LIST, LOG, SHOWS, TEMPEPS);

  { TForm1 }
  TForm1 = class(TForm)
    AboutDBGrid: TDBGrid;
    actClear: TAction;
    actDeleteIniFile: TAction;
    actOpenFolder: TAction;
    actZipSources: TAction;
    actSouceCodeFont: TAction;
    actCustom: TAction;
    actExit: TAction;
    actFilterEditCancel: TAction;
    actFind: TAction;
    actFormFonts: TAction;
    actHints: TAction;
    ActionList: TActionList;
    actLookupEps: TAction;
    actMeasure: TAction;
    actMemoToEps: TAction;
    actNew: TAction;
    actNext: TAction;
    actOpen: TAction;
    actPrior: TAction;
    actReadOnly: TAction;
    actRenameMatches: TAction;
    actSelect: TAction;
    actShowAllEpisodes: TAction;
    actStatusBarFont: TAction;
    actSysMenu: TAction;
    actTest: TAction;
    actUniqueToggle: TAction;
    actZap: TAction;
    BitBtn1: TBitBtn;
    BitBtnCancel: TBitBtn;
    BitBtnClear: TBitBtn;
    BitBtnCopyEps: TBitBtn;
    BitBtnExit: TBitBtn;
    BitBtnFindN: TBitBtn;
    BitBtnIcon: TBitBtn;
    BitBtnLookupEps: TBitBtn;
    BitBtnOpen: TBitBtn;
    BitBtnOpenReadme: TBitBtn;
    BitBtnRename: TBitBtn;
    BitBtnSelect: TBitBtn;
    BitBtnUndo: TBitBtn;
    btBack: TSpeedButton;
    btForward: TSpeedButton;
    btGo: TSpeedButton;
    DBGridEps: TDBGrid;
    DBGridList: TDBGrid;
    DBGridLog: TDBGrid;
    DBGridShows: TDBGrid;
    DBNavigatorShows: TDBNavigator;
    IpHtmlPanelPrivacy: TIpHtmlPanel;
    MenuItem9: TMenuItem;
    MenuItemOpenFolder: TMenuItem;
    MenuItem2: TMenuItem;
    MenuZipSources: TMenuItem;
    MenuItemInsert: TMenuItem;
    MenuItemDeleteShow: TMenuItem;
    MenuShowsToggleHidden: TMenuItem;
    DBNavigatorEps: TDBNavigator;
    DBNavigatorLog: TDBNavigator;
    edUrl: TEdit;
    FindDialog: TFindDialog;
    FontDialog: TFontDialog;
    ImageListFiley: TImageList;
    IpFileDataProvider1: TIpFileDataProvider;
    IpHtmlPanelReadme: TIpHtmlPanel;
    IpHtmlPanelAttribution: TIpHtmlPanel;
    IpHtmlPanelBrowser: TIpHtmlPanel;
    IpHttpDataProvider1: TIpHttpDataProvider;
    MenuItemTest: TMenuItem;
    PanelNav: TPanel;
    PopupMenuAbout: TPopupMenu;
    RichMemoReadme: TIpHtmlPanel;
    MainMenu: TMainMenu;
    Memo1: TMemo;
    MemoLicense: TMemo;
    MenuEps: TPopupMenu;
    MenuItem1: TMenuItem;
    MenuItemSourceCodeFont: TMenuItem;
    MenuShowEnv: TMenuItem;
    MenuItem3: TMenuItem;
    MenuItem4: TMenuItem;
    MenuItem5: TMenuItem;
    MenuItem6: TMenuItem;
    MenuItem7: TMenuItem;
    MenuItem8: TMenuItem;
    MenuItemSourceCodeFind: TMenuItem;
    MenuItemExit: TMenuItem;
    MenuItemMeasure: TMenuItem;
    MenuItemOpen: TMenuItem;
    MenuItemSeparator: TMenuItem;
    MenuItemToggle: TMenuItem;
    MenuItemUnique: TMenuItem;
    MenuItemZap: TMenuItem;
    MenuMaintenance: TMenuItem;
    MenuShows: TPopupMenu;
    MenuSourceCode: TPopupMenu;
    N1: TMenuItem;
    OpenDialog: TOpenDialog;
    PageControlAbout: TPageControl;
    PageControlMain: TPageControl;
    PageControlSourceCode: TPageControl;
    PanelAttribution: TPanel;
    PanelEpsBottom: TPanel;
    PanelEpsTop: TPanel;
    PanelFilesBottom: TPanel;
    PanelFilesGrid: TPanel;
    PanelFilesMain: TPanel;
    PanelLogTop: TPanel;
    PanelSelect: TPanel;
    PanelShowsBottom: TPanel;
    PanelShowsCombo: TPanel;
    PanelShowsNav: TPanel;
    PanelShowsTop: TPanel;
    PanelTop: TPanel;
    pnTop: TPanel;
    Separator1: TMenuItem;
    Separator2: TMenuItem;
    ShowDBFsStatus: TMenuItem;
    ShowsFilterEdit: TEditButton;
    StatusBar: TStatusBar;
    SynFreePascalSyn1: TSynFreePascalSyn;
    sysAbout: TAction;
    TabSheetPrivacy: TTabSheet;
    TabSheetAbout: TTabSheet;
    TabSheetBrowser: TTabSheet;
    TabSheetEps: TTabSheet;
    TabSheetInfo: TTabSheet;
    TabSheetLicense: TTabSheet;
    TabSheetList: TTabSheet;
    TabSheetLog: TTabSheet;
    TabSheetMemo: TTabSheet;
    TabSheetShows: TTabSheet;
    TabSheetSourceCode: TTabSheet;
    TabSheetReadme: TTabSheet;
    TaskDialog: TTaskDialog;
    ToolBarFiles: TToolBar;
    ToolBarMemo: TToolBar;
    ToolBarMemo1: TToolBar;
    procedure AboutDBGridMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure acBackExecute(Sender: TObject);
    procedure acForwardExecute(Sender: TObject);
    procedure actClearExecute(Sender: TObject);
    procedure actDeleteIniFileExecute(Sender: TObject);
    procedure actOpenFolderExecute(Sender: TObject);
    procedure actSouceCodeFontExecute(Sender: TObject);
    procedure actExitExecute(Sender: TObject);
    procedure actFilterEditCancelExecute(Sender: TObject);
    procedure actFindExecute(Sender: TObject);
    procedure actFormFontsExecute(Sender: TObject);
    procedure actGoExecute(Sender: TObject);
    procedure actHintsExecute(Sender: TObject);
    procedure actLookupEpsExecute(Sender: TObject);
    procedure actMeasureExecute(Sender: TObject);
    procedure actMemoToEpsExecute(Sender: TObject);
    procedure actOpenExecute(Sender: TObject);
    procedure actReadOnlyExecute(Sender: TObject);
    procedure actRenameMatchesExecute(Sender: TObject);
    procedure actSelectExecute(Sender: TObject);
    procedure actShowAllEpisodesExecute(Sender: TObject);
    procedure actStatusBarFontExecute(Sender: TObject);
    procedure actTestExecute(Sender: TObject);
    procedure actUniqueToggleExecute(Sender: TObject);
    procedure actZapExecute(Sender: TObject);
    procedure actZipSourcesExecute(Sender: TObject);
    procedure AddFiles(FileStrings: TStrings);
    function AddTabSheetSource(ntbk: TPageControl; TabCaption: String): TTabSheet;
    procedure AdjustColumns(Sender: TObject);
    procedure AssignDBFDataSource(var aDB: TDbf; var aDataSource: TDataSource);
    procedure AssignFoundEp(Sender: TObject);
    procedure AssignListFieldIfEmpty(sFieldName: String; sValue: String);
    procedure AssignRootNameToAssociatedFiles;
    function BackupFileForWrite(const FileName: String): Integer;
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtnCancelClick(Sender: TObject);
    procedure BitBtnNextClick(Sender: TObject);
    procedure BitBtnPrevClick(Sender: TObject);
    procedure BitBtnIconClick(Sender: TObject);
    procedure BitBtnLookupEpsMouseEnter(Sender: TObject);
    procedure BitBtnOpenReadmeClick(Sender: TObject);
    procedure BitBtnUndoClick(Sender: TObject);
    //function BoundsToDelimited(r: TRect): string;
    function BrowseToURL(var sURL: String; lastURL: String): Boolean;
    procedure btGoClick(Sender: TObject);
    procedure CheckLogCheckBoxes;
    procedure CheckResourceFiles;
    procedure CloseFreeAndNilDBF(var aDB: TDbf);
    procedure CloseGrid(var aGrid: TDBGrid);
    procedure ConfirmThenZapPackTable(var aDB: TDbf);
    function ContainsText(const AText, ASubText: String): Boolean;
    function MakeSourceSheets: Integer;
    function CopyFileForceDir(aSource: String; aDest: String): Boolean;
    procedure CreateDesktopShortcut;
    procedure CreateLinuxShortcut;
    {$IFDEF WINDOWS}
    procedure CreateWindowsShortcut;
    procedure CreateWineShortcut;
    {$ENDIF}
    procedure DBGridDblClick(Sender: TObject);
    procedure DBGridEpsEnter(Sender: TObject);
    procedure DBGridExit(Sender: TObject);
    procedure DBGridListAfterScroll(DataSet: TDataSet);
    procedure DBGridListBeforeScroll(DataSet: TDataSet);
    procedure DBGridListSelectEditor(Sender: TObject; Column: TColumn;
      var Editor: TWinControl);
    procedure DBGridListSetColumnWidths;
    procedure DBGridLogCellClick(Column: TColumn);
    procedure DBGridMakeHint(Sender: TObject; X, Y: Integer);
    procedure DBGridMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
    procedure DBGridShowsAfterScroll(DataSet: TDataSet);
    procedure DBGridShowsBeforeScroll(DataSet: TDataSet);
    procedure DBGridTitleClick(Column: TColumn);
    procedure DBNavigatorEpsClick(Sender: TObject; Button: TDBNavButtonType);
    procedure DBNavigatorEpsMouseEnter(Sender: TObject);
    procedure DBNavigatorLogClick(Sender: TObject; Button: TDBNavButtonType);
    procedure DBNavigatorLogMouseEnter(Sender: TObject);
    procedure DealWithSpecialFiles;
    function DeleteIfExists(aFileName: String): Boolean;
    //function DelimitedToBounds(s: string): TRect;
    procedure DoCreateOutZipStream(Sender: TObject; var AStream: TStream;
      AItem: TFullZipFileEntry);
    procedure DoDoneOutZipStream(Sender: TObject; var AStream: TStream;
      AItem: TFullZipFileEntry);
    procedure DownLoadEps(Sender: TObject);
    procedure DownloadShowsIfEmpty;
    procedure EditAndPost(var aDB: TDbf; aField, sValue: String);
    procedure EditButtonClick(Sender: TObject);
    procedure EditChange(Sender: TObject);
    procedure EditKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure EditKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure EditMouseEnter(Sender: TObject);
    procedure edUrlEditingDone(Sender: TObject);
    procedure edUrlMouseEnter(Sender: TObject);
    procedure EpsAfterScroll(DataSet: TDataSet);
    function ExtractAfterStr(sFind, s: String): String;
    function ExtractResourceToFile(aFileName: String): Boolean;
    procedure ExtractFileFromZip(aArchiveFileName, aDiskFileName: String);
    procedure ExtractResourceToObject(aFileName: String; aObject: TObject);
    function ExtractZipResourceIfNotFound(aResourceName: String): Boolean;
    function FileExists(const Path: String): Boolean;
    function FileExistsStr(const aCaseSensitiveFileName: String): Boolean;
    procedure FilterEpsOnTitle(DataSet: TDataSet; var Accept: Boolean);
    procedure FilterLogOnUndo(DataSet: TDataSet; var Accept: Boolean);
    procedure FilterShowsOnDirInTitle(DataSet: TDataSet; var Accept: Boolean);
    procedure FilterShowsOnTitleDir(DataSet: TDataSet; var Accept: Boolean);
    procedure FindDialogFind(Sender: TObject);
    function FindMyTitle(sFind: String): String;
    function FindPathTo(sFind: String): String;
    function FindTitleByEpName(Sender: TObject): String;
    function FindYear(sInputname: String): String;
    function FindYearExec(sInputname, sExpr: String): String;
    function FindYearPart(sInputname: String): String;
    function FixChars(s: String): String;
    function FixWinFileName(s: String): String;
    procedure FormActivate(Sender: TObject);
    procedure FormClose(Sender: TObject; var CloseAction: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormDropFiles(Sender: TObject; const FileNames: array of String);
    procedure FormResize(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormShowHint(Sender: TObject; HintInfo: PHintInfo);
    function GetDBFStatus(aDB: TDbf): String;
    function GetDeepestDir(const aFilename: String): String;
    function GetEpName(const curFile: String): String;
    function GetEpNum(const curFile: String): String;
    procedure GetMainAttributes(curFile: String);
    function HackFrom(sFind: String; var s, sLeft, sRight: String): String;
    function HackTo(sFind: String; var s, sLeft, sRight: String): String;
    procedure IncreaseAllFonts(Amount: Integer);
    procedure InitializeDBFs;
    procedure IpHtmlPanelBrowserDocumentOpen(Sender: TObject);
    procedure LoadDbfFromCSVFile(var aDB: TDbf; AFilename: String;
      ADelimiter: Char = ','; WithHeader: Boolean = True; AddRows: Boolean = True);
    procedure LogAfterPost(DataSet: TDataSet);
    procedure LogAfterScroll(DataSet: TDataSet);
    procedure LogBeforeEdit(DataSet: TDataSet);
    procedure LogBeforeScroll(DataSet: TDataSet);
    procedure MenuItemDeleteShowClick(Sender: TObject);
    procedure MenuItemInsertClick(Sender: TObject);
    procedure MenuShowEnvClick(Sender: TObject);
    procedure MenuShowsToggleHiddenClick(Sender: TObject);
    function MessageDlgEx(const AMsg: String; ADlgType: TMsgDlgType;
      AButtons: TMsgDlgButtons; AParent: TForm): TModalResult;
    procedure MessageList(title: String; sl: TStringList);
    procedure MessageRenamedCount(const buttonSelected: Integer);
    procedure MouseWheelScaleToFont(Sender: TObject; Shift: TShiftState;
      WheelDelta: Integer; MousePos: TPoint; var Handled: Boolean);
    function OSVersion: String;
    procedure PackThisTable(var aDB: TDbf);
    procedure PageControlAboutChange(Sender: TObject);
    procedure PageControlMainChange(Sender: TObject);
    procedure PageControlMainResize(Sender: TObject);
    procedure PageControlSourceCodeChange(Sender: TObject);
    procedure ParseFilesList;
    procedure ParseHTMLToMemo(Sender: TObject);
    function ParseTitleNEpNum(const curFile: String): String;
    procedure PostRecord(var aDB: TDbf);
    function ProbeFile(sFind: String): String;
    function QueryThenRename: Boolean;
    function RemoveAllSpacesAndDots(s: String): String;
    function RemovePunctuation(s: String): String;
    function RenameListItem(aDB: TDbf;
      sFromPath, sFrom, sToPath, sTo: String): Boolean;
    procedure RenameThisList(aDB: TDbf);
    procedure RightMouseFonts(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    function RunEx(sCommandLine: String; bRedirectErr: Boolean = False): String;
    function sAddHint(aDB: TDbf; FieldNum: Integer): String;
    function SafeGo(var aDB: TDbf; n: Longint): Boolean;
    procedure SelectAShow;
    procedure SetAllCursors(const CursorConstant: Integer);
    procedure SetBitBtnUndoEnabled;
    procedure SetBounds(aControl: TControl; ALeft, ATop, AWidth, AHeight: Integer);
      overload;
    procedure SetFilterEpsOnTitle;
    procedure SetListTextWidth;
    procedure SetRefreshButton(Navigator: TDBNavigator);
    procedure SetTabSheetListButtons;
    procedure SetTabSheetMemoButtons(Sender: TObject);
    procedure SetTabSheetSourceCodeButtons(Sender: TObject);
    procedure SetupDbAbout;
    procedure SetupDbEps;
    procedure SetupDbList;
    procedure SetupDbLog;
    procedure SetupDbShows;
    function sField(sFieldName: String): String;
    function ShowDataSetStates(DataSet: TDataSet): String;
    procedure ShowDBFsStatusClick(Sender: TObject);
    procedure ShowDBFStatus(aDB: TDbf);
    procedure ShowMessageList(var aList: TStringList);
    procedure ShowsFilterEditButtonClick(Sender: TObject);
    procedure ShowsFilterEditChange(Sender: TObject);
    procedure ShowsFilterEditEditingDone(Sender: TObject);
    procedure ShowsFilterEditKeyPress(Sender: TObject; var Key: Char);
    procedure ShowsFilterEditKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ShowStatusDBF(var aDB: TDbf; aFilterDescrip: String);
    procedure StatusBarDrawPanel(statBar: TStatusBar; Panel: TStatusPanel;
      const Rect: TRect);
    procedure StatusBarMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    function StripAlpha(s: String): String;
    function StripNonAlphaNum(s: String): String;
    function StripBadChars(s: String): String;
    procedure SwapFromTo;
    function sYearAndEpNumOnlyNoEpName: String;
    procedure SynEditKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure TabSheetAboutShow(Sender: TObject);
    procedure TabSheetAlignButtons;
    procedure TabSheetBrowserResize(Sender: TObject);
    procedure TabSheetEpsResize(Sender: TObject);
    procedure TabSheetEpsShow(Sender: TObject);
    procedure TabSheetInfoResize(Sender: TObject);
    procedure TabSheetInfoShow(Sender: TObject);
    procedure TabSheetListResize(Sender: TObject);
    procedure TabSheetListShow(Sender: TObject);
    procedure TabSheetLogResize(Sender: TObject);
    procedure TabSheetLogShow(Sender: TObject);
    procedure TabSheetMemoResize(Sender: TObject);
    procedure TabSheetPrivacyShow(Sender: TObject);
    procedure TabSheetReadmeShow(Sender: TObject);
    procedure TabSheetShowResource(aObject: TObject; aFileName: String);
    procedure TabSheetShowsResize(Sender: TObject);
    procedure TabSheetShowsShow(Sender: TObject);
    procedure TabSheetSourceCodeShow(Sender: TObject);
    procedure TestKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure ToolBarFilesResize(Sender: TObject);
    function ToolBarWidthSize(aToolBar: TToolbar): Integer;
    function TrimPeriod(s: String): String;
    procedure UpdateStatusBar;
    procedure ZapPackTable(var aDB: TDbf);

  public
    AllShowsTxt: String;
    buttonSelected: Integer;
    CopiedCount: Integer;
    D: array[Ord(Low(DBFs))..Ord(High(DBFs))] of DBRecord;
    EpGuidesURL: String;
    FileNameFFProbe: String;
    FileNamelibeay: String;
    FileNameMKVPropEdit: String;
    FileNamessleay: String;
    ConfigFileDir: String;
    FilePathFFProbe: String;
    FilePathlibeay: String;
    FilePathMKVPropEdit: String;
    FilePathssleay: String;
    FoundTitle: String;
    H: TIpHtml;
    IniFile: TIniFile;
    IniFileName: String;
    IsCancel: Boolean;
    IsCreateDesktopShortcut: Boolean;
    IsFormSnapped: Boolean;
    IsIniFileDeleted: Boolean;
    IsInternetConnected: Boolean;
    IsHideRenamedCount: Boolean;
    IsHideSelectAShow: Boolean;
    IsHideZipCount: Boolean;
    IsLinux: Boolean;
    IsModalWait: Boolean;
    IsWindows: Boolean;
    IsWine: Boolean;
    LastKeyTyped: Char;
    LastTitleFound: String;
    LastURL: String;
    MyComponent: TComponent;
    MyObject: TObject;
    OpDir: String;
    OpPath: String;
    ProgramPath: String;
    sCurDB: String;
    SearchDirectionForward: Boolean;
    SelectedTitle: String;
    sEpName: String;
    sEpNum: String;
    sFrom: String;
    sFromPath: String;
    SkipCount: Integer;
    SourceFilesCount: Integer;
    SourcePath: String;
    sTitle: String;
    sTo: String;
    sToday: String;
    sToPath: String;
    sURL: String;
    sYear: String;
    WasSelected: Boolean;
  end;

  TMyTestCase = class(TTestCase)
  published
    procedure should_be_exactly_1_TGUITestRunner;
    procedure should_return_2_forms;
  end;

  TTestDatabases = class(TTestCase)
  published
    procedure does_DBF_Exist;
  end;

  { TCheckInternet }
  TCheckInternet = class(TFPHTTPClient)
  public
    class function Available(AServer: String = 'www.google.com'): Boolean;
  end;  // TCheckInternet


{interface global forward declarations}
function IfThen(AValue: Boolean): String; overload;
procedure NoOp;
function ShowMessage(aMessage: String; bIsHidden: Boolean): Boolean; overload;

var
  Form1: TForm1;
  MyGuiTestRunner: TMyGUITestRunner;
  MyTestDatabase: TTestDatabases;
  IsExit: Boolean = False;
  RegEx: TRegExpr; //global, reduce timewasting repeated creates/destroys
  APieces: TStringList;
  sWineFFProbe: String = '';
  sWineMKVPropEdit: String = '';

const
  CR = Char(13);
  DirSep: String = DirectorySeparator;
  iABOUT = 0;
  iEPS = 1;
  iLIST = 2;
  iLOG = 3;
  iSHOWS = 4;
  iTEMPEPS = 5;
  LF = LineEnding;
  QuoteChr: Char = '"';
  VideoFileExtensions: String =
    '.ASF.WMV.AVI.DIVX.EVO.F4V.FLV.MKV.MVK.M4V.MP4.MPG.MPEG.M2P.TS.M2TS.OGG.OGV.OGX.MOV.QT.RMVB.VOB.WEBM';

implementation

{$R *.lfm}

{ TCheckInternet }
//class hack exposes protected procedure TFPHTTPClient.ConnectToServer
class function TCheckInternet.Available(AServer: String): Boolean;
var
  c: TCheckInternet;
begin
  Result := False;
  c := TCheckInternet.Create(nil);
  try
    try
      c.ConnectToServer(AServer, 80);
      Result := True;
    except
    end;
  finally
    c.Free;
  end;
end;     // TCheckInternet.Available

function HasInternetConnection: Boolean;
begin
  Result := TCheckInternet.Available();
end;     // IsInternetConnected

function IfThen(AValue: Boolean): String;
begin
  if avalue then
    Result := 'True'
  else
    Result := 'False';
end;


//NoOp eliminates Jedi Code Format Warnings about missing Else in case stmts
procedure NoOp;
begin
  asm
           NOP
  end;
end;


{ TODO : Consider what conditions we actually want to Test for, especially database integrity. }
{*************************** TTestDatabases *****************************}
procedure TTestDatabases.does_DBF_Exist;
//var ATest: TTestCase;
const
  aFileName = 'ALLSHOWS.DBF';
begin
  //ATest:= TTestCase.CreateWithName('Test '+aFileName+' Exists');
  //RegisterTest('Test Databases',ATest);
  //Status(aFileName + ' ' + IfThen(FileExists(aFileName), 'does ', 'DOES NOT ')+'exist.');
  Assert(FileExists(aFileName), 'DBF doesnt exist:' + aFileName);
end;

{************************* TMyTestCase ********************************}
procedure TMyTestCase.should_be_exactly_1_TGUITestRunner;
var
  FormsCount: Integer = 0;
  i: Integer = 0;
begin
  for i := 0 to Application.ComponentCount - 1 do
    if Application.Components[i].ToString = 'TGUITestRunner' then
      FormsCount := FormsCount + 1;
  AssertEquals('There should be exactly one instance of TGUITestRunner', 1, FormsCount);
end;

procedure TMyTestCase.should_return_2_forms;
var
  i: Integer = 0;
  s: String = '';
  //FormsCount : integer = 0;
begin
  for i := 0 to Application.ComponentCount - 1 do
    //if IsPublishedProp(Application.Components[i], 'Form') then
    //FormsCount:= FormsCount +1;
    s := s + Application.Components[i].ToString + #9 +
      Application.Components[i].Name + LineEnding;
  s := s + 'ComponentCount= ' + Application.ComponentCount.ToString;
  ShowMessage(s);
  //Check(Application.ComponentCount=2, 'There should be two forms');
  Check(True, 'showing components');
end;

{**********************  TMyGuiTestRunner ****************}
procedure TMyGuiTestRunner.FormClose(Sender: TObject; var Closeaction: Tcloseaction);
begin
  CloseAction := caFree;
  MyGuiTestRunner := nil;
end;

{************************* TForm1 ***********************}
procedure TForm1.acBackExecute(Sender: TObject);
begin
  IpHtmlPanelBrowser.GoBack;
end;

procedure TForm1.AboutDBGridMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);

var
  col: Integer = 0;
  j: Integer = 0;
  rec: Integer = 0;
  row: Integer = 0;
  menuItem: TMenuItem;
  s: String = '';

begin
  inherited;
  if Button = mbRight then
    if TDBGrid(Sender).Columns.Count > 0 then
      with TDBGrid(Sender) do
      begin
        MouseToCell(X, Y, col, row);
        if dgTitles in Options then
          Dec(row);
        if dgIndicator in Options then
          Dec(col);
        {$warnings off}//yup, I know TDBGrid and TMyGrid are not "related"
        if TMyGrid(TDBGrid(Sender)).DataLink.Active and (row >= 0) and (col >= 0) then
        begin
          TMyGrid(TDBGrid(Sender)).DataLink.ActiveRecord := row;
         {$warnings on}
          s := TDbf(DataSource.DataSet).FieldByName('Value').AsString;
          s := ExpandFileName(s);
          OpenDialog.FileName := s;
          MenuItemOpenFolder.Caption := 'Open ' + s;
          if FileExists(s) or DirectoryExists(s) then PopupMenuAbout.PopUp;
        end;
      end;
end;

procedure TForm1.acForwardExecute(Sender: TObject);
begin
  IpHtmlPanelBrowser.GoForward;
end;

procedure TForm1.actClearExecute(Sender: TObject);
begin
  DBGridList.Enabled := False;
  ZapPackTable(D[iList].aDB);
  ShowsFilterEdit.Text := EmptyStr;
  SetTabSheetListButtons;
  DBGridList.Hint := 'Drop here files to be renamed';
  DBGridList.Enabled := True;
  FoundTitle := '';
  LastTitleFound := '';
  sTitle := '';
  sEpNum := '';
  sEpName := '';
  sYear := '';
  SelectedTitle := EmptyStr;
end;

procedure TForm1.actDeleteIniFileExecute(Sender: TObject);
begin
  if FileExists(IniFileName) then
    if DeleteFile(IniFileName) then IsIniFileDeleted := True;
end;

procedure TForm1.actOpenFolderExecute(Sender: TObject);
var
  s: String;
begin
  s := OpenDialog.FileName;
  if (not DirectoryExists(s)) and
    (not AnsiContainsText('.ini.txt', ExtractFileExt(s)))
  then s := ExtractFilePath(s);
  if DirectoryExists(s) or FileExists(s) then OpenDocument(s);
end;

procedure TForm1.actSouceCodeFontExecute(Sender: TObject);
var
  i: Integer = 0;
begin
  if PageControlSourceCode.PageCount > 0 then
    with PageControlSourceCode.Page[PageControlSourceCode.ActivePageIndex] do
      for i := 0 to ComponentCount - 1 do
        if Components[i] is TSynEdit then
        begin
          FontDialog.Font := TSynEdit(Components[i]).Font;
          if TSynEdit(Components[i]).Font.Size < 6 then
            FontDialog.Font.Size := 10;
          FontDialog.Title := 'Change Editor Font';
          FontDialog.Options := [fdFixedPitchOnly, fdForceFontExist, fdLimitSize];
          if Form1.FontDialog.Execute then
            TSynEdit(Components[i]).Font := Form1.FontDialog.Font;
        end;
end;

procedure TForm1.actExitExecute(Sender: TObject);
begin
  IsExit := True;
  BitBtnCancelClick(Sender);
  SetAllCursors(crDefault);
  Form1.Close;
  Application.Terminate;
end;

procedure TForm1.actFilterEditCancelExecute(Sender: TObject);
begin
  ShowsFilterEdit.Text := EmptyStr;
  D[iEps].aFilterDescription := EmptyStr;
  D[iEps].aFilterText := EmptyStr;
  D[iEps].aDB.Filtered := False;
  D[iShows].aFilterDescription := EmptyStr;
  D[iShows].aFilterText := EmptyStr;
  D[iShows].aDB.Filtered := False;
  PanelSelect.Visible := False;
  IsModalWait := False;
end;

procedure TForm1.actFindExecute(Sender: TObject);
var
  FocusedComponent: Integer = 0;
  i: Integer = 0;
begin
  //Which component currently has the focus, for Find & Replace function
  //repeat
  //  if Form1.Components[i] is TWinControl then
  //    if TWinControl(Form1.Components[i]).Focused then
  //      FocusedComponent := i;
  //  Inc(i);
  //until (i = Form1.ComponentCount - 1) or (FocusedComponent > 0);
  //if FocusedComponent > 0 then
  //begin
  //  ShowMessage('Focused Component= ' + Form1.Components[FocusedComponent].Name);
  //  FindDialog.Execute;
  //end;
  with TObject(Sender) do
    if (ClassName = 'TTabSheet') and Showing and Visible then
      SetFocus;
end;

procedure TForm1.actFormFontsExecute(Sender: TObject);
var
  i: Integer = 0;
begin
  FontDialog.Font := Form1.Canvas.Font;
  FontDialog.Title := 'Change Main Fonts';
  if FontDialog.Execute then
  begin
    for i := 0 to Form1.ComponentCount - 1 do
      if (TComponent(Form1.Components[i]) is TControl) then
        if not (TComponent(Form1.Components[i]) is TSynEdit) then
          TControl(Form1.Components[i]).Font := FontDialog.Font;
    Form1.Canvas.Font := FontDialog.Font;
  end;
end;

procedure TForm1.actGoExecute(Sender: TObject);
begin
  try
    BrowseToURL(sURL, lastURL);
  finally
  end;
end;

procedure TForm1.actHintsExecute(Sender: TObject);
var
  i: Integer = 0;
begin
  for i := 0 to ToolBarFiles.ControlCount - 1 do
    ToolBarFiles.Controls[i].AutoSize := True;
  for i := 0 to Form1.ComponentCount - 1 do
  begin
    if IsPublishedProp(Form1.Components[i], 'ShowHint') then
      SetPropValue(Form1.Components[i], 'ShowHint', True);
    if IsPublishedProp(Form1.Components[i], 'ParentShowHint') then
      SetPropValue(Form1.Components[i], 'ParentShowHint', True);
  end;
end;

procedure TForm1.actLookupEpsExecute(Sender: TObject);
begin
  D[iEps].aDB.DisableControls;
  D[iList].aDB.First;
  with D[iList].aDB do
    while (not EOF) and (not IsExit) do
    begin
      Application.ProcessMessages;
      //skip if already looked up
      //if LastTitleFound = D[iShows].aDB.FieldByName('Title').AsString then FoundTitle := LastTitleFound else

      //already Manually Selected a Show
      if SelectedTitle <> EmptyStr then
      begin
        FoundTitle := SelectedTitle;
        LastTitleFound := SelectedTitle;
      end;

      if (trim(FieldByName('To').AsString) = EmptyStr) and (not IsExit) then
      begin
        //only lookup if it's different Title from previous Episode
        if (not IsExit) and ((LastTitleFound = EmptyStr) or
          (trim(FieldByName('To').AsString) = EmptyStr)) then
        begin
          D[iShows].aFilterText := FieldByName('Title').AsString;
          FoundTitle := FindMyTitle(D[iShows].aFilterText);

          //maybe deepest dir has the Title
          if FoundTitle = EmptyStr then
            FoundTitle := FindMyTitle(GetDeepestDir(FieldByName('FROMPATH').AsString));

          //try finding Title by matching the epname and epnumber
          if (FoundTitle = EmptyStr) then
            FoundTitle := FindTitleByEpName(Sender);

          //title might also require the year of the series
          if FoundTitle = EmptyStr then
            FoundTitle := FindMyTitle(sYearAndEpNumOnlyNoEpName);

          //try finding Title by LastTitleFound
          if (FoundTitle = EmptyStr) then
            FoundTitle := FindMyTitle(LastTitleFound);

          //try again searching by Title
          if FoundTitle = EmptyStr then
            FoundTitle := FindMyTitle(FieldByName('Title').AsString);

          //still not found, manually select a show
          if (FoundTitle = EmptyStr) and (D[iShows].aDB.ExactRecordCount > 1) then
            SelectAShow;
        end;
        if (FoundTitle <> EmptyStr) then
        begin
          AssignFoundEp(Sender);
          D[iEps].aDB.Filtered := True;
          PageControlMain.ActivePageIndex := TabSheetList.PageIndex;
        end;
      end; {if}
      Next;
    end;
  //if manually Select a Show, then recurse this procedure again
  if WasSelected then
  begin
    WasSelected := False;
    actLookupEpsExecute(Sender); //recursive
  end;
  SetTabSheetListButtons;
  D[iEps].aDB.EnableControls;
  SetAllCursors(crDefault);
end;

procedure TForm1.actMeasureExecute(Sender: TObject);
var
  curWidth: Integer = 0;
  DForm: TDialogForm = nil;
  i: array[0..15] of Integer;
  iPos: Integer = 0;
  k: Integer = 0;
  OutStrings: TStringList = nil;
  PForm: TProgressForm = nil;
  s: String = '';
  w: array[0..15] of Integer;
begin
  with D[iShows].aDB do
  begin
    PForm := TProgressForm.CreateParented(Form1.Handle);
    PForm.BitBtn.Kind := bkCancel;
    PForm.Show;
    PForm.ProgressBar1.Max := PhysicalRecordCount;
    PForm.ProgressBar1.Position := 0;
    PForm.HeaderPanel.Caption := 'Measure widths of actual data in fields';
    PForm.BitBtn.Visible := True;
    for k := 0 to 15 do
      w[k] := 0;
    PForm.Label1.Font.Size := 20;
    repeat
      for k := 0 to FieldCount - 1 do
      begin
        i[k] := length(trim(Fields[k].AsString));
        if i[k] > w[k] then
          w[k] := i[k];
      end;
      Next;
      PForm.ProgressBar1.Position := iPos;
      PForm.Label1.Caption := IntToStr(PhysicalRecNo) + '/' +
        IntToStr(PhysicalRecordCount);
      PForm.Label1.Left := (PForm.PanelProgress.Width - PForm.Label1.Width) div 2;
      PForm.HeaderPanel.Caption := 'Measure field widths';
      if PForm.ModalResult = mrCancel then
        Break;
      Inc(iPos);
    until (EOF) or (PForm.ModalResult = mrCancel);
    PForm.Close;
    with DForm do
    begin
      DForm := TDialogForm.Create(Form1);
      DForm.Font := StatusBar.Canvas.Font;
      OutStrings := TStringList.Create;
      OutStrings.Add(#9 + 'Minimum required field widths:');
      OutStrings.Add(EmptyStr);
      for k := 0 to FieldCount - 1 do
      begin
        s := #9 + Format('%-11s', [Fields[k].FieldName]) + '= ' + #9 +
          Format('%4d', [w[k]]) + ' chars';
        OutStrings.Add(s);
      end;
      for k := 0 to OutStrings.Count - 1 do
      begin
        curWidth := Max(curWidth, DForm.Canvas.TextWidth(OutStrings[k]));
        DForm.DialogMemo.Lines.Add(OutStrings[k]);
      end;
      DForm.Width := 390;
      DForm.Height := 390;
      DForm.Caption := 'Longest widths of fields:';
      DForm.Position := poDesigned;
      DForm.ShowModal;
      FreeAndNil(OutStrings);
    end;
  end;
end;

procedure TForm1.actMemoToEpsExecute(Sender: TObject);
var
  EpIsFound: Boolean = False;
  MyVarArray: Variant;
  s: String = '';
  s1: String = '';
  s2: String = '';
  s3: String = '';
begin
  if Memo1.GetTextLen > 0 then
    with D[iTempEps].aDB do
    begin
      D[iEps].aDB.Active := True;
      D[iTempEps].aDB.Active := True;
      D[iTempEps].aDB.First;
      while not D[iTempEps].aDB.EOF do
      begin
        if D[iTempEps].aDB.FieldByName('Title').AsString = '' then
          D[iTempEps].aDB.FieldByName('Title').AsString :=
            D[iShows].aDB.FieldByName('Title').AsString;
        s1 := trim(D[iTempEps].aDB.FieldByName('Title').AsString);
        s2 := trim(D[iTempEps].aDB.FieldByName('EpNum').AsString);
        MyVarArray := VarArrayOf([s1, s2]);
        //turn off Filtering to allow Locate to work correctly
        D[iEps].aDB.Filtered := False;
        EpIsFound := D[iEps].aDB.Locate('Title;EpNum', MyVarArray, [loCaseInsensitive]);
        //ShowMessage('s1= '+s1+lf+'s2= '+s2+lf+'EpIsFound= '+ifthen(episfound,'true','false'));
        if not EpIsFound then
          with D[iEps].aDB do
          begin
            D[iEps].aDB.DisableControls;
            D[iEps].aDB.ReadOnly := False;  //set this while controls are disabled
            try
              s1 := FixChars(D[iTempEps].aDB.FieldByName('Title').AsString);
              s2 := RemoveAllSpacesAndDots(
                FixChars(D[iTempEps].aDB.FieldByName('EpNum').AsString));
              s3 := FixChars(D[iTempEps].aDB.FieldByName('EpName').AsString);
              D[iEps].aDB.Exclusive := True;
              D[iEps].aDB.Append;
              D[iEps].aDB.Edit;
              D[iEps].aDB.FieldByName('Title').AsString := s1;
              D[iEps].aDB.FieldByName('EpNum').AsString := s2;
              D[iEps].aDB.FieldByName('EpName').AsString := s3;
              FieldByName('SortOrder').AsString := StripNonAlphaNum(s1 + s2 + s3);
              PostRecord(D[iEps].aDB);
            except
            end;
            D[iTempEps].aDB.EnableControls;
            D[iEps].aDB.EnableControls;
          end;
        D[iTempEps].aDB.Next;
      end;
      D[iEps].aDB.RegenerateIndexes;
    end;
end;

procedure TForm1.actOpenExecute(Sender: TObject);
begin
  if OpenDialog.Execute then
    AddFiles(openDialog.Files);
end;

procedure TForm1.actReadOnlyExecute(Sender: TObject);
var
  i: Integer = 0;
begin
  for i := 0 to Form1.ComponentCount - 1 do
    if IsPublishedProp(Form1.Components[i], 'ReadOnly') then
      SetPropValue(Form1.Components[i], 'ReadOnly', False);
  DBGridShows.Options := DBGridShows.Options - [dgRowSelect] +
    [dgEditing] + [dgCellHints];
  DBGridShows.ReadOnly := not DBGridShows.ReadOnly;
  D[iEps].aDB.ReadOnly := not D[iEps].aDB.ReadOnly;
  DBGridEps.ReadOnly := not DBGridEps.ReadOnly;
  D[iTempEps].aDB.ReadOnly := not D[iTempEps].aDB.ReadOnly;
  D[iList].aDB.ReadOnly := not D[iList].aDB.ReadOnly;
  with D[iShows].aDB do
  begin
    ReadOnly := not ReadOnly;
    for i := 0 to FieldCount - 1 do
      Fields[i].ReadOnly := not Fields[i].ReadOnly;
  end;
  for i := 0 to D[iEps].aDB.FieldCount - 1 do
    D[iEps].aDB.Fields[i].ReadOnly := not D[iEps].aDB.Fields[i].ReadOnly;
  UpdateStatusBar;
end;

procedure TForm1.actRenameMatchesExecute(Sender: TObject);
begin
  RenameThisList(D[iList].aDB);
  actClearExecute(D[iList].aDB);
end;

procedure TForm1.actSelectExecute(Sender: TObject);
begin
  PageControlMain.ActivePageIndex := TabSheetEps.TabIndex;
  SetAllCursors(crHourGlass);
  D[iEps].aDB.DisableControls;
  with D[iShows].aDB do
    if IsInternetConnected then
      if FieldByName('LookUpDate').AsString <> sToday then
      begin
        sURL := EpGuidesURL + FieldByName('Directory').AsString + '/';
        BrowseToURL(sURL, lastURL);
        ParseHTMLToMemo(Sender);
        actMemoToEpsExecute(Form1);
        EditAndPost(D[iShows].aDB, 'LookUpDate', sToday);
      end;
  FoundTitle := D[iShows].aDB.FieldByName('Title').AsString;
  LastTitleFound := FoundTitle;
  D[iShows].aFilterText := FoundTitle;
  D[iList].aDB.Edit;
  D[iList].aDB.FieldByName('Title').AsString := FoundTitle;
  D[iList].aDB.Post;
  SetFilterEpsOnTitle;
  AdjustColumns(DBGridEps);
  D[iShows].aDB.EnableControls;
  D[iEps].aDB.EnableControls;
  SetAllCursors(crDefault);
  WasSelected := True; //user manually selected a Show
  if WasSelected then SelectedTitle := FoundTitle;
  IsModalWait := False;
  if not IsInternetConnected then StatusBar.Panels[0].Text :=
      'EPISODES NOT UPDATED!  --  NO INTERNET CONNECTION!';
end;

procedure TForm1.actShowAllEpisodesExecute(Sender: TObject);
begin
  with DBGridEps do
  begin
    D[iEps].aFilterText := EmptyStr;
    D[iEps].aFilterDescription := EmptyStr;
    D[iEps].aDB.Filtered := False;
    Columns[3].Visible := not Columns[3].Visible;
    Columns[2].SizePriority := IfThen(Columns[3].Visible, 0, 1);
    AdjustColumns(DBGridEps);
    UpdateStatusBar;
  end;
end;

procedure TForm1.actStatusBarFontExecute(Sender: TObject);
begin
  FontDialog.Title := 'Change StatusBar Font';
  FontDialog.Font := StatusBar.Canvas.Font;
  FontDialog.Options := [fdFixedPitchOnly, fdForceFontExist, fdLimitSize];
  if FontDialog.Execute then
    StatusBar.Canvas.Font := FontDialog.Font;
end;

procedure TForm1.actTestExecute(Sender: TObject);
begin
  if (MyGuiTestRunner = nil) then
    Application.CreateForm(TGUITestRunner, MyGuiTestRunner);
  MyGuiTestRunner.Show;
  //MyGuiTestRunner.MemoLog.Lines.Add();
end;

procedure TForm1.actUniqueToggleExecute(Sender: TObject);
begin
  //with DbfShows.Indexes[0] do
  //if ixUnique in Options
  //  then Options:= Options - [ixUnique]
  //  else Options:= Options - [ixUnique];
  //DbfShows.Active:= false;
  ////DbfShows.IndexName:= '';
  //DbfShows.UpdateIndexDefs;
  ////DbfShows.RegenerateIndexes;
  ////DbfShows.IndexName:= 'TITLEDIR';
  //DbfShows.Active:= true;
  NoOp;
end;

procedure TForm1.actZapExecute(Sender: TObject);
begin
  if MessageDlgEx('Zap?' + LineEnding +
    'Delete, Re-Download, and then re-create the Shows and Episodes databases?',
    mtConfirmation, [mbYes, mbCancel], Form1) = mrYes then
    with D[iShows].aDB, DBGridShows do
    begin
      DisableControls;
      StatusBar.Panels[0].Text := 'ZAPPING...';
      ZapPackTable(D[iShows].aDB);
      ZapPackTable(D[iEps].aDB);
      actFilterEditCancelExecute(Form1);
      if IsInternetConnected then
        DeleteIfExists(OpPath + AllShowsTxt);
      Options := Options + [dgRowSelect] + [dgCellHints];
      EnableControls;
      InitializeDBFs;
      DownloadShowsIfEmpty;
      First;
    end;
end;

procedure TForm1.actZipSourcesExecute(Sender: TObject);

var
  z: TZipper;
  k: Integer = 0;
  sAdded: String = '';

  procedure AddIfExists(s: String);
  begin
    Application.ProcessMessages;
    k := z.Entries.Count;
    s := ExpandFileName(s);
    //ArchivefileName automatically replaces Windows path delimiters as required
    if FileExists(s) then z.Entries.AddFileEntry(s, ExtractFileName(s));
    if z.Entries.Count = k + 1 then
      sAdded := sAdded + LF + s;
  end;

begin
  z := TZipper.Create;
  try
    Form1.SetAllCursors(crHourGlass);
    z.FileName := SourcePath + 'filey-src.zip';
    if FileExists(z.FileName) then
      DeleteFile(z.FileName);
    if FileExists(OpPath + 'filey-src.zip') then
      DeleteFile(OpPath + 'filey-src.zip');
    AddIfExists(SourcePath + 'fileyfrm.pas');
    AddIfExists(SourcePath + 'fileyfrm.lfm');
    AddIfExists(SourcePath + 'filey.lpi');
    AddIfExists(SourcePath + 'filey.lpr');
    AddIfExists(SourcePath + 'fileydialog.pas');
    AddIfExists(SourcePath + 'fileydialog.lfm');
    AddIfExists(SourcePath + 'fileyprogress.pas');
    AddIfExists(SourcePath + 'fileyprogress.lfm');
    AddIfExists(SourcePath + 'filey64.png');
    AddIfExists(SourcePath + '..' + DirSep + 'Privacy_Policy.html');
    AddIfExists(SourcePath + '..' + DirSep + 'README.html');
    AddIfExists(SourcePath + '..' + DirSep + 'README.md');
    AddIfExists(SourcePath + 'resources' + DirSep + 'ffprobe.exe');
    AddIfExists(SourcePath + 'resources' + DirSep + 'libeay32.dll');
    AddIfExists(SourcePath + 'resources' + DirSep + 'mkvpropedit.exe');
    AddIfExists(SourcePath + 'resources' + DirSep + 'ssleay32.dll');
    z.ZipAllFiles;
  finally
    Form1.SetAllCursors(crDefault);
    ShowMessage(sAdded + LF + LF + z.Entries.Count.ToString +
      ' files zipped to: ' + z.FileName, IsHideZipCount);
    z.Free;
  end;
end;

procedure TForm1.AddFiles(FileStrings: TStrings);
var
  i: Integer = 0;
begin
  PageControlMain.ActivePageIndex := TabSheetList.PageIndex;
  Form1.SetAllCursors(crHourGlass);
  with D[iList].aDB do
  begin
    IndexName := '';
    First;
    DBGridList.DataSource := D[iList].aDataSource;
    Form1.BringToFront;  //let's see what's being dropped
    for i := 0 to FileStrings.Count - 1 do
    begin
      Application.ProcessMessages;
      Exclusive := True;
      Append;
      Edit;
      FieldByName('FROM').AsString := FileStrings[i];
      PostRecord(D[iList].aDB);
    end;
    DealWithSpecialFiles;
    ParseFilesList;
    Form1.SetAllCursors(crDefault);
  end;
end;

procedure TForm1.AdjustColumns(Sender: TObject);
begin
  if Sender is TDBGrid then
    with TDBGrid(Sender) do
      if (Columns.Count > 0) and (Enabled) and (Showing) then
        AutoAdjustColumns;
end;

procedure TForm1.AssignDBFDataSource(var aDB: TDbf; var aDataSource: TDataSource);
begin
  if (aDataSource = nil) and (not assigned(aDataSource)) then
    aDataSource := TDataSource.Create(Form1);
  aDataSource.AutoEdit := True;
  aDataSource.DataSet := aDB;
  aDataSource.Name := ExtractFileNameOnly('DataSource' + aDB.TableName);
  aDataSource.Enabled := True; //enabled MUST come last in this set, or mem leaks
end;

procedure TForm1.AssignFoundEp(Sender: TObject);
var
  EpIsFound: Boolean = False;
  MyVarArray: Variant;
  s: String = '';
  s1: String = '';
  s2: String = '';
begin
  with D[iList].aDB do
  begin
    LastTitleFound := FieldByName('Title').AsString;
    ShowsFilterEdit.Text := FoundTitle;
    s1 := trim(FoundTitle);
    s2 := trim(FieldByName('EpNum').AsString);
    MyVarArray := VarArrayOf([s1, s2]);
    //turn off Filtering to allow Locate to work correctly
    D[iEps].aDB.Filtered := False;
    EpIsFound := D[iEps].aDB.Locate('Title;EpNum', MyVarArray, [loCaseInsensitive]);
    if (not EpIsFound) then
      DownloadEps(Sender); //download eps, try again
    EpIsFound := D[iEps].aDB.Locate('Title;EpNum', MyVarArray, []);
    D[iList].aDB.Edit;
    if EpIsFound then
    begin
      FieldByName('Title').AsString := D[iEps].aDB.FieldByName('Title').AsString;
      FieldByName('EpName').AsString :=
        ReplaceStr(D[iEps].aDB.FieldByName('EpName').AsString, ' ', '.');
      FieldByName('Directory').AsString :=
        D[iShows].aDB.FieldByName('Directory').AsString;
    end;
    s := sField('Title') + sField('Epnum') + sField('EpName') +
      sField('Height') + sField('Codec') + sField('AudioChan') +
      sField('Subtitles') + D[iList].aDB.FieldByName('Extension').AsString;
    Exclusive := True;
    if D[iList].aDB.FieldByName('TreeLevel').AsString = '0' then
      D[iList].aDB.FieldByName('To').AsString := FixWinFileName(s);
    PostRecord(D[iList].aDB);
    //D[iEps].aDB.Filtered := True;
    //PageControlMain.ActivePageIndex := TabSheetList.PageIndex;
  end;
end;

procedure TForm1.AssignListFieldIfEmpty(sFieldName: String; sValue: String);
begin
  with D[iList].aDB do
    if FieldByName(sFieldName).AsString = EmptyStr then
      FieldByName(sFieldName).AsString := sValue;
end;

procedure TForm1.AssignRootNameToAssociatedFiles;
var
  RootTo: String = '';
begin
  with D[iList].aDB do
  begin
    IndexName := 'SORTORDER';
    First;
    while not EOF do
    begin
      if FieldByName('TREELEVEL').AsString = '0' then
        RootTo := ExtractFileNameOnly(FieldByName('To').AsString);
      if FieldByName('TREELEVEL').AsString = '1' then
      begin
        Edit;
        FieldByName('TO').AsString :=
          FixWinFileName(RootTo + FieldByName('Extension').AsString);
        PostRecord(D[iList].aDB);
      end;
      Next;
    end;
  end;
end;

function TForm1.BackupFileForWrite(const FileName: String): Integer;
const
  //DefaultBackupAddExt = 'bak';
  MaxCounter = 9;
var
  aText: String = '';
  BackupFileName: String = '';
  CounterFileName: String = '';
  i: Integer = 0;
begin
  Result := 0;
  BackupFilename := OpPath + 'backup' + PathDelim + ExtractFileName(FileName) + ';';
  // rename all backup files (increase number)
  i := 1;
  while FileExistsUTF8(BackupFilename + IntToStr(i)) and (i <= MaxCounter) do
    Inc(i);
  if i > MaxCounter then
  begin
    Dec(i);
    CounterFilename := BackupFilename + IntToStr(MaxCounter);
    // remove old backup file
    repeat
      if FileExistsUTF8(CounterFilename) then
        if not DeleteFileUTF8(CounterFilename) then
        begin
          AText := Format('Unable to remove old backup file "%s"!', [CounterFilename]);
          Result := MessageDlgEx(aText, mtError, [mbAbort, mbRetry, mbIgnore], Form1);
          if Result = mrAbort then
            exit;
          if Result = mrIgnore then
            Result := mrOk;
        end;
    until Result <> mrRetry;
  end;
  // rename all old backup files
  Dec(i);
  while i >= 1 do
  begin
    repeat
      if not RenameFileUTF8(BackupFilename + IntToStr(i), BackupFilename +
        IntToStr(i + 1)) then
      begin
        AText := Format('Unable to rename file "%s" to "%s"!',
          [BackupFilename + IntToStr(i), BackupFilename + IntToStr(i + 1)]);
        Result := MessageDlgEx(aText, mtError, [mbAbort, mbRetry, mbIgnore], Form1);
        if Result = mrAbort then
          exit;
        if Result = mrIgnore then
          Result := mrOk;
      end;
    until Result <> mrRetry;
    Dec(i);
  end;
  BackupFilename := BackupFilename + '1';
  repeat
    if not CopyFile(Filename, CounterFilename) then
    begin
      AText := Format('Unable to backup file "%s" to "%s"!',
        [FileName, CounterFilename]);
      Result := MessageDlgEx(aText, mtError, [mbAbort, mbRetry, mbIgnore], Form1);
      if Result = mrAbort then
        exit;
      if Result = mrIgnore then
        Result := mrOk;
    end
    else
      Result := mrOk;
  until Result <> mrRetry;
end;

procedure TForm1.BitBtn1Click(Sender: TObject);
begin
  OpenURL('https://github.com/IndySockets/OpenSSL-Binaries');
end;

procedure TForm1.BitBtnCancelClick(Sender: TObject);
begin
  ShowsFilterEdit.Text := EmptyStr;
  D[iEps].aFilterText := EmptyStr;
  D[iEps].aFilterDescription := EmptyStr;
  D[iEps].aDB.Filtered := False;
  D[iShows].aFilterText := EmptyStr;
  D[iShows].aFilterDescription := EmptyStr;
  D[iShows].aDB.Filtered := False;
  PanelSelect.Visible := False;
  SetAllCursors(crDefault);
  IsExit := True;
end;

procedure TForm1.BitBtnNextClick(Sender: TObject);
var
  ASearch: String = '';
  s: String = '';
  p: Integer = 0;
begin
  p := PageControlSourceCode.ActivePageIndex;
  s := 'EditButton' + p.ToString;
  ASearch := TEditButton(PageControlSourceCode.Page[p].FindComponent(s)).Text;
  s := 'SynEdit' + p.ToString;
  if not SearchDirectionForward then
  begin
    TSynEdit(PageControlSourceCode.Page[p].FindComponent(s)).SearchReplace(
      ASearch, '', []);
    SearchDirectionForward := True;
  end;
  TSynEdit(PageControlSourceCode.Page[p].FindComponent(s)).SearchReplace(
    ASearch, '', []);
  SearchDirectionForward := True;
end;

procedure TForm1.BitBtnPrevClick(Sender: TObject);
var
  ASearch: String;
  s: String;
  pg: Integer;
begin
  pg := PageControlSourceCode.ActivePageIndex;
  s := 'EditButton' + pg.ToString;
  ASearch := TEditButton(PageControlSourceCode.Page[pg].FindComponent(s)).Text;
  s := 'SynEdit' + pg.ToString;
  if SearchDirectionForward then
  begin
    TSynEdit(PageControlSourceCode.Page[pg].FindComponent(s)).SearchReplace(
      ASearch, '', [ssoBackwards]);
    SearchDirectionForward := False;
  end;
  TSynEdit(PageControlSourceCode.Page[pg].FindComponent(s)).SearchReplace(ASearch,
    '', [ssoBackwards]);
  SearchDirectionForward := False;
end;

procedure TForm1.BitBtnIconClick(Sender: TObject);
begin
  OpenURL('https://www.flaticon.com/free-icon/television_926087');
end;

procedure TForm1.BitBtnLookupEpsMouseEnter(Sender: TObject);
begin
  SetTabSheetListButtons;
end;

procedure TForm1.BitBtnOpenReadmeClick(Sender: TObject);
begin
  OpenDocument(OpPath + 'README.html');
end;

procedure TForm1.BitBtnUndoClick(Sender: TObject);
begin
  with D[iLog].aDB do
  begin
    ShowDeleted := True;
    OnFilterRecord := @FilterLogOnUndo;
    Filtered := True;
    FindFirst;
    RenameThisList(D[iLog].aDB);
    if RecordCount > 0 then
      PackTable;
    ShowDeleted := False;
    Filtered := False;
    FindFirst;
  end;
end;

//function TForm1.BoundsToDelimited(r: TRect): string;
//var
//  strList: TStringList;
//begin
//  strList := TStringList.Create;
//  strList.Add(IntToStr(r.Left));
//  strList.Add(IntToStr(r.Top));
//  strList.Add(IntToStr(r.Right));
//  strList.Add(IntToStr(r.Bottom));
//  Result := strList.DelimitedText;
//  strList.Free;
//end;

function TForm1.BrowseToURL(var sURL: String; lastURL: String): Boolean;
begin
  if IsInternetConnected then
  begin
    if LastURL <> sURL then //we haven't already downloaded this page
      if trim(sURL) <> EmptyStr then
      begin
        if trim(sURL) <> '' then
          edUrl.Text := sURL;
        //if not ContainsText(sURL, EpGuidesURL) then sURL := EpGuidesURL + sURL;
        if sURL[length(sURL)] <> '/' then
          sURL := sURL + '/';
        if ContainsText(sURL, 'http') then
          try
            IpHtmlPanelBrowser.DataProvider := IpHttpDataProvider1;
            IpHtmlPanelBrowser.OpenURL(sURL);
            LastURL := sURL;
            Result := True;
            IpHtmlPanelBrowser.DataProvider := nil;
          except
            on E: Exception do
            begin
              MessageDlg('Unable to open URL', 'sURL: ' + sURL +
                'Error: ' + E.Message, mtError, [mbCancel], 0);
              sURL := '';
              Application.ProcessMessages;
              Result := False;
            end;
          end;
      end;
  end;
end;

procedure TForm1.btGoClick(Sender: TObject);
begin
  if trim(edUrl.Text) <> '' then
    try
      actGoExecute(Sender);
    finally
    end;
end;

procedure TForm1.CheckLogCheckBoxes;
var
  aBookMark: TBookmark = nil;
begin
  BitBtnUndo.Enabled := False;
  with D[iLog].aDB do
  begin
    DisableControls;
    aBookMark := GetBookmark;
    First;
    while not EOF do
    begin
      if FieldByName('Undo').AsBoolean = True then BitBtnUndo.Enabled := True;
      Next;
    end;
    GotoBookmark(aBookMark);
    EnableControls;
  end;
end;

function TForm1.AddTabSheetSource(ntbk: TPageControl; TabCaption: String): TTabSheet;
var
  Panel: TPanel;
  SynEdit: TSynEdit;
  EditButton: TEditButton;
  BitBtnNext: TBitBtn;
  BitBtnPrev: TBitBtn;
  p: Integer;
begin
  Result := TTabSheet.Create(PageControlSourceCode);
  Result.PageControl := ntbk;
  Result.Caption := TabCaption;
  if ContainsText('.pas,.lpr', lowercase(ExtractFileExt(TabCaption))) then
    Result.ImageIndex := 1
  else
    Result.ImageIndex := 0;
  if SourceFilesCount > 0 then
  begin
    p := PageControlSourceCode.PageCount - 1; //currentpage

    Panel := TPanel.Create(PageControlSourceCode.Pages[p]);
    Panel.Parent := Result;
    Panel.Align := alTop;
    Panel.AutoSize := False;

    EditButton := TEditButton.Create(PageControlSourceCode.Pages[p]);
    with EditButton do
    begin
      Align := alLeft;
      AutoSelect := True;
      AutoSize := True;
      ButtonHint := 'Clear Find';
      ButtonWidth := 38;
      DirectInput := True;
      EchoMode := emNormal;
      Enabled := True;
      Height := 44;
      Hint := 'Clear Find';
      Hint := 'Find Text';
      ImageIndex := 3;
      Images := ImageListFiley;
      ImageWidth := 32;
      Left := 1;
      MaxLength := 0;
      Name := 'EditButton' + p.ToString;
      Caption := EmptyStr; //Caption MUST come AFTER Name
      NumGlyphs := 1;
      OnButtonClick := @EditButtonClick;
      OnChange := @EditChange;
      OnKeyDown := @EditKeyDown;
      OnKeyUp := @EditKeyUp;
      OnMouseEnter := @EditMouseEnter;
      Parent := Panel;
      PasswordChar := #0;
      TabOrder := 1;
      Top := 1;
      Width := TabSheetAbout.ClientWidth div 2;
    end;

    BitBtnNext := TBitBtn.Create(PageControlSourceCode.Pages[p]);
    with BitBtnNext do
    begin
      Align := alLeft;
      AutoSize := True;
      BorderSpacing.CellAlignHorizontal := ccaFill;
      BorderSpacing.CellAlignVertical := ccaFill;
      GlyphShowMode := gsmApplication;
      Height := 50;
      Hint := 'Find Next';
      ImageIndex := 18;
      Images := ImageListFiley;
      ImageWidth := 32;
      Kind := bkCustom;
      Left := 241;
      Margin := -1;
      Name := 'BitBtnNext' + p.ToString;
      Caption := EmptyStr; //Caption MUST come AFTER Name
      OnClick := @BitBtnNextClick;
      Parent := Panel;
      Spacing := 4;
      TabOrder := 0;
      Top := 1;
      Width := 48;
    end;

    BitBtnPrev := TBitBtn.Create(PageControlSourceCode.Pages[p]);
    with BitBtnPrev do
    begin
      Align := alLeft;
      AutoSize := True;
      BorderSpacing.CellAlignHorizontal := ccaFill;
      BorderSpacing.CellAlignVertical := ccaFill;
      GlyphShowMode := gsmAlways; //gsmApplication;
      Height := 44;
      Hint := 'Find Previous';
      ImageIndex := 19;
      Images := ImageListFiley;
      ImageWidth := 32;
      Kind := bkCustom;
      Left := 289;
      Margin := -1;
      Name := 'BitBtnPrev' + p.ToString;
      Caption := EmptyStr; //Caption MUST come AFTER Name
      NumGlyphs := 1;
      OnClick := @BitBtnPrevClick;
      Parent := Panel;
      Spacing := 4;
      TabOrder := 2;
      Top := 1;
      Width := 48;
    end;

    with SynEdit do
    begin
      SynEdit := TSynEdit.Create(PageControlSourceCode.Pages[p]);
      Align := alClient;
      //Font.Name:= 'Consolas';
      //Font.Size:= 13;
      //Font.Orientation:= 0;
      //Font.Color:= clBlack;
      Font := StatusBar.Canvas.Font;
      Font.Style := [];
      Font.Pitch := fpFixed;
      Font.SetDefault;
      Highlighter := SynFreePascalSyn1;
      Hint := 'Source Code';
      Name := 'SynEdit' + p.ToString;
      Caption := EmptyStr; //Caption MUST come AFTER Name
      Parent := Result;
      PopupMenu := MenuSourceCode;
      ReadOnly := True;
      SynEdit.MouseOptions := [emCtrlWheelZoom];
      SynEdit.OnKeyDown := @SynEditKeyDown;
      Width := ClientWidth;
    end;

    ExtractResourceToObject(TabCaption, SynEdit);
    //end;
  end;
end;

function TForm1.MakeSourceSheets: Integer;
var
  ZipFile: TUnZipper;
  s: String = '';
  i: Integer = 0;
begin
  ZipFile := TUnZipper.Create;
  SourceFilesCount := 0;
  try
    ZipFile.FileName := OpPath + 'filey-src.zip';
    ZipFile.OnCreateStream := @DoCreateOutZipStream;
    ZipFile.OnDoneStream := @DoDoneOutZipStream;
    ZipFile.Examine;
    if ZipFile.Entries.Count > 0 then
      for i := 0 to ZipFile.Entries.Count - 1 do
      begin
        s := ZipFile.Entries[i].ArchiveFileName;
        if ContainsText('.lpi,.lpr,.pas,.lfm', ExtractFileExt(s)) then
        begin
          Inc(SourceFilesCount);
          AddTabSheetSource(PageControlSourceCode, s);
        end;
      end;
  finally
    ZipFile.Free;
  end;
end;

procedure TForm1.CheckResourceFiles;
var
  i: Integer = 0;
begin
  ExtractZipResourceIfNotFound('filey-src.zip');
  //libeay32.dll and ssleay32.dll are needed for Windows
  if IsWindows then
  begin
    FileNamelibeay := ProgramPath + 'libeay32.dll';
    ExtractResourceToFile(FileNamelibeay);
    FilePathlibeay := ExpandFileName(FindPathTo(FileNamelibeay));
    FileNamessleay := ProgramPath + 'ssleay32.dll';
    ExtractResourceToFile(FileNamessleay);
    FilePathssleay := ExpandFileName(FindPathTo(FileNamessleay));
  end;

  //use Linux executable if found, otherwise wine the Windows version
  if IsLinux and (FindPathTo('ffprobe') <> EmptyStr) then FileNameFFProbe := 'ffprobe'
  else
  begin
    sWineFFProbe := 'wine ';
    FileNameFFProbe := 'ffprobe.exe';
    ExtractResourceToFile(FileNameFFProbe);
  end;
  FilePathFFProbe := ExpandFileName(FindPathTo(FileNameFFProbe));

  if IsLinux and (FindPathTo('mkvpropedit') <> EmptyStr) then
    FileNameMKVPropedit := 'mkvpropedit'
  else
  begin
    sWineMKVPropedit := 'wine ';
    FileNameMKVPropedit := 'mkvpropedit.exe';
    ExtractResourceToFile(FileNameMKVPropEdit);
  end;
  FilePathMKVPropEdit := ExpandFileName(FindPathTo(FileNameMKVPropEdit));
  ExtractResourceToFile('README.html');
  ExtractResourceToFile('Privacy_Policy.html');
  ExtractResourceToFile('filey64.png');
  // PageControlSourceCode.ActivePageIndex := TabSheetFileyFrm.PageIndex;
  SourceFilesCount := MakeSourceSheets;
end;

procedure TForm1.CloseFreeAndNilDBF(var aDB: TDbf);
begin
  aDB.IndexName := '';
  aDB.Close;
  FreeAndNil(aDB);
end;

procedure TForm1.CloseGrid(var aGrid: TDBGrid);
begin
  aGrid.DataSource := nil;
  aGrid.Columns.Clear;
  FreeAndNil(aGrid);
end;

procedure TForm1.ConfirmThenZapPackTable(var aDB: TDbf);
var
  aBookMark: TBookMark = nil;
  lastTitle: String = '';
begin
  //don't bother if nothing to Zap
  if (aDB.PhysicalRecordCount > 0) then
    if MessageDlgEx('Are you CERTAIN that you want to Zap (completely delete) and refresh the '
      + aDB.TableName + ' database?', mtConfirmation, [mbCancel, mbYes], Form1) =
      mrYes then
    begin
      SetAllCursors(crHourGlass);
      aBookMark := D[iShows].aDB.GetBookmark;
      aDB.Filtered := False;
      aDB.First;
      if aDB.Name = 'EPS' then
        while not aDB.EOF do
        begin
          if (aDB.FieldByName('TITLE').AsString <> lastTitle) then
            if D[iShows].aDB.Locate('TITLE', aDB.FieldByName('TITLE').AsString, [])
            then
            begin
              //allow LookUp these episodes again Today
              D[iShows].aDB.Edit;
              D[iShows].aDB.FieldByName('LookUpDate').AsString := EmptyStr;
              D[iShows].aDB.Post;
              lastTitle := aDB.FieldByName('TITLE').AsString;
            end;
          aDB.Next;
        end;
      ZapPackTable(aDB);
      D[iShows].aFilterText := EmptyStr;
      D[iShows].aFilterDescription := EmptyStr;
      D[iEps].aFilterText := EmptyStr;
      D[iEps].aFilterDescription := EmptyStr;
      D[iShows].aDB.GotoBookmark(aBookMark);
      SetAllCursors(crDefault);
    end;
  SetRefreshButton(DBNavigatorEps);
end;

//AnsiContainsText is case insensitive, AnsiContainsStr is case sensitive
function TForm1.ContainsText(const AText, ASubText: String): Boolean;
begin
  Result := AnsiContainsText(AText, ASubText);
end;

procedure TForm1.CreateDesktopShortcut;
begin
  {$IFDEF WINDOWS}
  if IsWindows then
    if IsWine then CreateWineShortcut
    else
      CreateWindowsShortcut;
  {$ENDIF}
  if IsLinux then CreateLinuxShortcut;
end;

function TForm1.CopyFileForceDir(aSource: String; aDest: String): Boolean;
var
  aPath: String = '';
begin
  Result := False;
  aPath := ExtractFilePath(ExpandFileName(aDest));
  //make certain that Destination Path actually exists
  if not DirectoryExists(aPath) then ForceDirectories(aPath);
  Result := CopyFile(aSource, aDest, True, True);
end;

procedure TForm1.CreateLinuxShortcut;
var
  IconName: String = '';
  TargetName: String = '';
  FileVerInfo: TFileVersionInfo = nil;
begin
  TargetName := GetEnv('HOME') + PathDelim + 'Desktop' + PathDelim +
    ExtractFileNameOnly(Application.ExeName) + '.desktop';
  if (IsLinux and IsCreateDesktopShortcut) then
    if not FileExistsStr(TargetName) then
      with TStringList.Create do
        try
          IconName := GetEnv('HOME') + '/.local/share/icons/filey64.png';
          ExtractResourceToFile('FILEY64.PNG');
          if (not FileExists(IconName)) and FileExists('filey64.png') then
            CopyFileForceDir('filey64.png', IconName);
          Add('[Desktop Entry]');
          FileVerInfo := TFileVersionInfo.Create(Form1);
          FileVerInfo.ReadFileInfo;
          Add('Version=' + FileVerInfo.VersionStrings.Values['FileVersion']);
          Add('Encoding=UTF-8');
          Add('Type=Application');
          Add('Icon=' + IconName);
          Add('Path=' + ExtractFilePath(Application.ExeName));
          Add('Exec=' + ExtractFilePath(Application.ExeName) + 'filey');
          Add('Name=' + ExtractFileName(Application.ExeName));
          Add('GenericName=filey - Rename TV show files consistently');
          Add('Terminal=false');
          Add('Categories=Application;Utility;AudioVideo');
          Add('Comment=Rename TV show files consistently');
          SaveToFile(TargetName);
          //set to Allow Launching
          RunEx('gio set ' + TargetName + ' metadata::trusted true');
          RunEx('chmod +x ' + TargetName)
        finally
          FreeAndNil(FileVerInfo);
          Free;
        end;
end;

{$IFDEF WINDOWS}
procedure TForm1.CreateWindowsShortcut;
var
  s: string = '';
  TargetName: string = '';
  WineName: WideString = '';
  FileVerInfo: TFileVersionInfo = nil;
  AppDataPath: array[0..MaxPathLen] of char = #0; //Allocate memory
  InFolder: array[0..MAX_PATH] of char = #0;
  IObject: IUnknown = nil;
  IPFile: IPersistFile = nil;
  ISLink: IShellLink = nil;
  LinkName: WideString = '';
  PIDL: PItemIDList = nil;
begin
  TargetName := Application.ExeName;
  IsWine := GetEnv('WINEPREFIX') <> EmptyStr;
  IObject := CreateComObject(CLSID_ShellLink);
  ISLink := IObject as IShellLink;
  IPFile := IObject as IPersistFile;
  with ISLink do
  begin
    SetPath(PChar(TargetName));
    SetWorkingDirectory(PChar(ExtractFilePath(TargetName)));
    //to store the path
    AppDataPath := '';
    SHGetSpecialFolderPath(0, AppDataPath, CSIDL_DESKTOPDIRECTORY, False);
    LinkName := UTF8ToUTF16(AppDataPath + '\Filey.lnk');
    WineName := UTF8ToUTF16(AppDataPath + '\Filey.desktop');
    //ShowMessage('linkname= ' + linkname + lf + 'winename= ' + winename);
    //to create the shortcut
    if (not FileExists(UTF8Encode(WineName))) and
       (not FileExists(UTF8Encode(LinkName)) and
       (not FileExists('C:\Users\Public\Desktop\Filey.lnk')) and
       (IsCreateDesktopShortcut)) then
      IPFile.Save(PWChar(LinkName), False);
  end;
end;

procedure TForm1.CreateWineShortcut;
var
  s: string = '';
  outstring: string = '';
  TargetName: string = '';
  WineName: WideString = '';
  FileVerInfo: TFileVersionInfo = nil;
  AppDataPath: array[0..MaxPathLen] of char = #0; //Allocate memory
  InFolder: array[0..MAX_PATH] of char = #0;
  IObject: IUnknown = nil;
  IPFile: IPersistFile = nil;
  ISLink: IShellLink = nil;
  LinkName: WideString = '';
  PIDL: PItemIDList = nil;
begin
  TargetName := Application.ExeName;
  IsWine := GetEnv('WINEPREFIX') <> EmptyStr;
  IObject := CreateComObject(CLSID_ShellLink);
  ISLink := IObject as IShellLink;
  IPFile := IObject as IPersistFile;
  with ISLink do
  begin
    SetPath(PChar(TargetName));
    SetWorkingDirectory(PChar(ExtractFilePath(TargetName)));
    //to store the path
    AppDataPath := '';
    SHGetSpecialFolderPath(0, AppDataPath, CSIDL_DESKTOPDIRECTORY, False);
    LinkName := UTF8Decode(AppDataPath + '\Filey.lnk');
    WineName := UTF8Decode(AppDataPath + '\Filey.desktop');
    //ShowMessage('linkname= ' + linkname + lf + 'winename= ' + winename);
    //to create the shortcut
    if (not FileExists(UTF8Encode(WineName))) and
       (not FileExists(UTF8Encode(LinkName))) and (IsCreateDesktopShortcut) then
      IPFile.Save(PWChar(LinkName), False);
    if (FileExists(UTF8Encode(WineName))) and (FileExists(UTF8Encode(LinkName))) then DeleteFile(LinkName);
    //set to Allow Launching
    if (FileExists(UTF8Encode(WineName))) then
    begin
      s := GetEnv('OLDPWD') + '/' + 'Desktop/Filey.desktop';
      //ShowMessage('cmd= ' + 'gio set ' + s + ' metadata::trusted true');
      //ExecuteProcess(UTF8ToSys('gio set ' + s + ' metadata::trusted true'), '', []);
      RunCommand('gio set ' + s + ' metadata::trusted true', outstring);
      RunCommand('chmod a+x ' + s, outstring);
    end;
  end;
end;
{$ENDIF}


procedure TForm1.DbGridShowsAfterScroll(DataSet: TDataSet);
var
  curRec: Integer = 0;
begin
  //if DbfShows.ControlsDisabled = False then
  with D[iShows].aDB, D[iShows] do
    if (Active) and (D[iEps].aDB.Active) then
    begin
      sURL := EpGuidesURL + FieldByName('Directory').AsString + '/';
      if Assigned(D[iEps].aDB) then  {don't try this until after Eps is created}
      begin
        D[iEps].aFilterText := trim(FieldByName('TITLE').AsString);
        curRec := D[iEps].aDB.PhysicalRecNo;
        if not D[iEps].aDB.Locate('TITLE', FieldByName('TITLE').AsString, []) then
          D[iEps].aDB.PhysicalRecNo := curRec;  //Eps.physicalrecno?
      end;
      ShowStatusDBF(TDbf(DataSet), aFilterDescription);
    end;
end;

procedure TForm1.DBGridShowsBeforeScroll(DataSet: TDataSet);
begin
  ShowStatusDBF(TDbf(DataSet), D[iShows].aFilterDescription);
end;

procedure TForm1.DBGridDblClick(Sender: TObject);
begin
  if dgRowSelect in TDBGrid(Sender).Options then
    TDBGrid(Sender).Options :=
      TDBGrid(Sender).Options - [dgRowSelect] + [dgEditing] + [dgCellHints];
end;

procedure TForm1.DBGridEpsEnter(Sender: TObject);
begin
  ShowStatusDBF(D[iEps].aDB, D[iEps].aFilterDescription);
end;

procedure TForm1.DBGridExit(Sender: TObject);
begin
  UpdateStatusBar;
end;

procedure TForm1.DBGridListAfterScroll(DataSet: TDataSet);
begin
  ShowStatusDBF(TDbf(DataSet), D[iList].aFilterDescription);
end;

procedure TForm1.DBGridListBeforeScroll(DataSet: TDataSet);
begin
  ShowStatusDBF(TDbf(DataSet), D[iList].aFilterDescription);
end;

procedure TForm1.DBGridListSelectEditor(Sender: TObject; Column: TColumn;
  var Editor: TWinControl);
begin
  UpdateStatusBar;
end;

procedure TForm1.DBGridListSetColumnWidths;
begin
  if Assigned(DBGridList) then
    with DBGridList do
      if Showing then
        if Columns.Count = 2 then
          if Enabled then
          begin
            DisableAlign;
            Columns[0].Width := (Width div 2) - DEFINDICATORCOLWIDTH;
            Columns[1].Width := Columns[0].Width;
            EnableAlign;
          end;
  UpdateStatusBar;
end;

procedure TForm1.DBGridLogCellClick(Column: TColumn);
begin
  CheckLogCheckBoxes;
end;

procedure TForm1.DBGridMakeHint(Sender: TObject; X, Y: Integer);
var
  col: Integer = 0;
  j: Integer = 0;
  rec: Integer = 0;
  row: Integer = 0;
begin
  inherited;
  {$warnings off}//Yup, TDBGrid and TMyGrid are not related
  //if List.IsEmpty then DBGridList.Hint := 'Drop here files to be renamed';
  if TDBGrid(Sender).Columns.Count > 0 then
    with TDBGrid(Sender) do
    begin
      MouseToCell(X, Y, col, row);
      if dgTitles in Options then
        Dec(row);
      if dgIndicator in Options then
        Dec(col);
      if TMyGrid(TDBGrid(Sender)).DataLink.Active and (row >= 0) and (col >= 0) then
      begin
        rec := TMyGrid(TDBGrid(Sender)).DataLink.ActiveRecord;
        try
          TMyGrid(TDBGrid(Sender)).DataLink.ActiveRecord := row;
          Hint := Columns[col].Field.AsString;
          Hint := EmptyStr;
          for j := 0 to DataSource.DataSet.FieldCount - 1 do
            Hint := Hint + sAddHint(TDbf(DataSource.DataSet), j);
          Application.ActivateHint(Mouse.CursorPos);
        except
          //ShowMessage(Name + '.Columns.Count= ' + IntToStr(Columns.Count) + LineEnding + 'col= ' + IntToStr(col) + LineEnding);
        end;
        TMyGrid(TDBGrid(Sender)).DataLink.ActiveRecord := rec;
      end;
      {$warnings on}
      UpdateStatusBar;
    end;
end;

procedure TForm1.DBGridMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
begin
  inherited;
  DBGridMakeHint(Sender, X, Y);
end;

procedure TForm1.DBGridTitleClick(Column: TColumn);
begin
  AdjustColumns(Column.Grid);
end;

procedure TForm1.DBNavigatorEpsClick(Sender: TObject; Button: TDBNavButtonType);
begin
  begin
    case Button of
      nbEdit: DBGridEps.Options :=
          DBGridEps.Options - [dgRowSelect] + [dgEditing] + [dgCellHints];
      nbRefresh: ConfirmThenZapPackTable(D[iEps].aDB);
      else
        NoOp
    end;
  end;
  UpdateStatusBar;
end;

procedure TForm1.DBNavigatorEpsMouseEnter(Sender: TObject);
begin
  SetRefreshButton(DBNavigatorEps);
end;

procedure TForm1.DBNavigatorLogClick(Sender: TObject; Button: TDBNavButtonType);
begin
  case Button of
    nbEdit: DBGridLog.Options :=
        DBGridLog.Options - [dgRowSelect] + [dgEditing] + [dgCellHints];
    nbRefresh: ConfirmThenZapPackTable(D[iLog].aDB);
    else
      NoOp
  end;
  UpdateStatusBar;
end;

procedure TForm1.DBNavigatorLogMouseEnter(Sender: TObject);
begin
  SetRefreshButton(DBNavigatorLog);
end;

procedure TForm1.DealWithSpecialFiles;
{Go back through the list and look for names like 20070202.mkv and matching nfo files}
var
  FoundMatchingNFO: Boolean = False;
  SpecialRecNo: Integer = 0;
  sSpecialName: String = '';
  sTestExt: String = '';
  sTestName: String = '';
begin
  RegEx.Expression := '\d{8}';   //find: '10090902'
  with D[iList].aDB do
  begin
    First;
    while (not EOF) do
    begin
      sTestName := ExtractFileNameOnly(FieldByName('FROM').AsString);
      sTestExt := ExtractFileExt(FieldByName('FROM').AsString);
      APieces.Clear;
      RegEx.Exec(UTF8Encode(sTestName));
      sSpecialName := RegEx.Match[0]; //8-digit-only filename
      {A video file with filename of 8 digits only, looks like a special kind
       of file, look for matching .nfo file in this list beginning with the
       same number, followed by the series title epnum and epname}
      RegEx.Split(UTF8Encode(sTestName), APieces);
      if FieldByName('TO').AsString = EmptyStr then //didn't already find a match
        if (Pos(UpCase(sTestExt), VideoFileExtensions) > 0) and
          (sTestName = sSpecialName) and (not FoundMatchingNFO) then
        begin
          //sSpecialExt := sTestExt;
          SpecialRecNo := PhysicalRecNo;
          First;
          //this loop allows primary/secondary files to be out-of-order
          while (not EOF) and (not FoundMatchingNFO) do
          begin
            sTestName := ExtractFileNameOnly(FieldByName('FROM').AsString);
            sTestExt := ExtractFileExt(FieldByName('FROM').AsString);
            if (Pos(sSpecialName, sTestName) = 1) and
              (UpCase(sTestExt) = '.NFO') and
              (trim(FieldByName('TO').AsString) = EmptyStr) then
            begin
              APieces.Clear;
              RegEx.Split(UTF8Encode(sTestName), APieces);
              if (APieces[0] = EmptyStr) and (APieces[1] <> EmptyStr) then
              begin
                QueryThenRename;
                SafeGo(D[iList].aDB, SpecialRecNo); //go back to where we were working
                QueryThenRename;
              end;
              FoundMatchingNFO := True;
            end;
            Next;
          end;
        end;
      //SafeGo(List, SpecialRecNo); //causes endless looping
      Next;
    end;
  end;
  D[iList].aDB.First;
end;

function TForm1.DeleteIfExists(aFileName: String): Boolean;
begin
  Result := False;
  if FileExists(aFileName) then
    Result := DeleteFile(aFileName);
end;

//function TForm1.DelimitedToBounds(s: string): TRect;
//var
//  strList: TStringList;
//begin
//  Result := Bounds(0, 100, 200, 400);
//  strList := TStringList.Create;
//  strList.AddCommaText(s);
//  if strList.Count = 4 then
//    Result := Bounds(strList[0].ToInteger, strList[1].ToInteger,
//      strList[2].ToInteger, strList[3].ToInteger);
//  strList.Free;
//end;

procedure TForm1.DownLoadEps(Sender: TObject);
begin
  with D[iShows].aDB do
    if IsInternetConnected then
      if FieldByName('Title').AsString <> EmptyStr then
        //only do 1 Episodes lookup per Title per day
        if FieldByName('LookUpDate').AsString <> sToday then
        begin
          SetAllCursors(crHourGlass);
          sURL := EpGuidesURL + FieldByName('Directory').AsString + '/';
          BrowseToURL(sURL, lastURL);
          ParseHTMLToMemo(Sender);
          actMemoToEpsExecute(Application);
          EditAndPost(D[iShows].aDB, 'LookUpDate', sToday);
          D[iEps].aDB.FindFirst;
          AdjustColumns(DBGridEps);
          SetAllCursors(crDefault);
        end;
end;

procedure TForm1.DownloadShowsIfEmpty;
var
  ShowsMemo: TMemo = nil;
begin
  with D[iShows].aDB do
  begin
    if IsEmpty then
    begin
      if not FileExists(OpPath + AllShowsTxt) and (IsInternetConnected) then
      begin
        //ShowMessage('PhysicalRecordCount= ' + IntToStr(PhysicalRecordCount) + LF + 'FileExists=' + FilePathAllShowsTxt + '= ' + ifthen(FileExists(FilePathAllShowsTxt)));
        ShowsMemo := TMemo.Create(Form1);
        ShowsMemo.Lines.Clear;
        ShowsMemo.Lines.Add(TFPHTTPClient.SimpleGet(
          'https://epguides.com/common/allshows.txt'));
        ShowsMemo.Lines.SaveToFile(OpPath + AllShowsTxt);
        FreeAndNil(ShowsMemo);
      end;
      if FileExists(OpPath + AllShowsTxt) then
      begin
        if not IsInternetConnected then
          ShowMessage('WARNING: Using old "allshows.txt" because NO INTERNET CONNECTION.');
        IndexName := EmptyStr;
        IndexDefs.Update;
        if IndexDefs.Count > 0 then IndexDefs.Clear;
        LoadDBFFromCSVFile(D[iShows].aDB, OpPath + AllShowsTxt, ',', True, True);
      end;
    end;
  end;
end;

procedure TForm1.EditAndPost(var aDB: TDbf; aField, sValue: String);
var
  TempReadOnly: Boolean = False;
begin
  TempReadOnly := aDB.ReadOnly;
  aDB.ReadOnly := False;
  aDB.Edit;
  aDB.FieldByName(aField).AsString := sValue;
  PostRecord(aDB);
  aDB.ReadOnly := TempReadOnly;
end;

procedure TForm1.EditButtonClick(Sender: TObject);
begin
  //ShowMessage('Sender.ClassName = ' + Sender.ClassName);
  if Sender.ClassName = 'TEditButton' then
    TEditButton(Sender).Text := '';
  SearchDirectionForward := True;
  SetTabSheetMemoButtons(Sender);
end;

procedure TForm1.EditKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
var
  s: String = '';
  cp: Integer = 0;
begin
  cp := PageControlSourceCode.ActivePageIndex;
  s := TObject(Sender).ClassName + cp.ToString;
  s := RightStr(s, length(s) - 1); //chop off the 'T' from component name
  MyComponent := PageControlSourceCode.Page[cp].FindComponent(s);
  case Key of
    VK_RETURN: if SearchDirectionForward then BitBtnNextClick(Sender)
      else
        BitBtnPrevClick(Sender);
    VK_F3: if ssShift in Shift then BitBtnPrevClick(Sender)
      else
        BitBtnNextClick(Sender)
    else
      NoOp
  end; {case}
end;

procedure TForm1.EditKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  SetTabSheetMemoButtons(Sender);
end;

procedure TForm1.EditMouseEnter(Sender: TObject);
begin
  SetTabSheetSourceCodeButtons(Sender);
end;

procedure TForm1.edUrlEditingDone(Sender: TObject);
begin
  try
    btGoClick(Sender);
  finally
  end;
end;

procedure TForm1.edUrlMouseEnter(Sender: TObject);
begin
  edURL.Text := sURL;
end;

procedure TForm1.EpsAfterScroll(DataSet: TDataSet);
begin
  ShowStatusDBF(TDbf(DataSet), D[iEps].aFilterDescription);
end;

function TForm1.ExtractAfterStr(sFind, s: String): String;
var
  k: Integer = 0;
begin
  k := Pos(sFind, s) + length(sFind);
  if k > 0 then
    Result := ExtractSubStr(s, k, [#10, #13])
  else
    Result := EmptyStr;
end;

procedure TForm1.ExtractFileFromZip(aArchiveFileName, aDiskFileName: String);
var
  ZipFile: TUnZipper;
  sl: TStringList;
begin
  sl := TStringList.Create;
  ZipFile := TUnZipper.Create;
  try
    ZipFile.FileName := OpPath + 'filey-src.zip';
    ZipFile.OnCreateStream := @DoCreateOutZipStream;
    ZipFile.OnDoneStream := @DoDoneOutZipStream;
    OpenDialog.FileName := aDiskFileName;
    sl.Add(aArchiveFileName);
    Zipfile.UnZipFiles(sl);
  finally
    ZipFile.Free;
    sl.Free;
  end;
end;

function TForm1.ExtractResourceToFile(aFileName: String): Boolean;
var
  IsFound: Boolean = False;
begin
  Result := False;
  ////if Linux resources not already on computer, then just use Windows versions
  //if (aFileName= 'ffprobe') or (aFileName='mkvpropedit') then
  //  aFileName:= aFileName + '.exe';
  IsFound := (FileSearch(aFileName, OpPath, [sfoImplicitCurrentDir])) <> EmptyStr;
  if not IsFound then
  begin
    //use OpenDialog as a container to hold Extraction PATH + Filename
    MyObject := TObject(OpenDialog);
    ExtractFileFromZip(aFileName, OpPath + aFileName);
  end;
end;

procedure TForm1.ExtractResourceToObject(aFileName: String; aObject: TObject);
var
  S: TResourceStream = nil;
begin
  //don't waste time loading resource if user doesn't look at it
  if ((aObject is TSynEdit) and (TSynEdit(aObject).Lines.Count = 0)) or
    ((aObject is TIpHtmlPanel) and (TIpHtmlPanel(aObject).GetContentSize.Width = 0)) or
    ((aObject is TOpenDialog) and (not FileExists(TOpenDialog(aObject).FileName))) then
  begin
    S := TResourceStream.Create(HInstance, 'FILEY-SRC.ZIP',
      {$IFDEF WINDOWS}Windows.{$ENDIF}
      RT_RCDATA);
    //tell ExtractFileFromZip to send it to screen, not file
    MyObject := aObject;
    ExtractFileFromZip(aFileName, OpPath + aFileName);
    S.Free;
  end;
end;

procedure TForm1.DoCreateOutZipStream(Sender: TObject; var AStream: TStream;
  AItem: TFullZipFileEntry);
begin
  AStream := TMemorystream.Create;
end;

procedure TForm1.DoDoneOutZipStream(Sender: TObject; var AStream: TStream;
  AItem: TFullZipFileEntry);
begin
  AStream.Position := 0;
  //use OpenDialog as a container to hold Extraction PATH + Filename
  case TObject(MyObject).ClassName of
    'TSynEdit': TSynEdit(MyObject).Lines.LoadFromStream(AStream);
    'TIpHtmlPanel': TIpHTMLPanel(MyObject).SetHtmlFromStream(AStream);
    'TOpenDialog':
      with TFileStream.Create(TOpenDialog(MyObject).FileName, fmCreate) do
      begin
        CopyFrom(AStream, AStream.Size);
        Free; // destroy the file stream
      end;
  end; {case}
  AStream.Free;
end;

procedure TForm1.EditChange(Sender: TObject);
var
  ASearch: String;
  p: Integer;
  s: String;
begin
  p := PageControlSourceCode.ActivePageIndex;
  s := 'EditButton' + p.ToString;
  ASearch := TEditButton(PageControlSourceCode.Page[p].FindComponent(s)).Text;
  s := 'SynEdit' + p.ToString;
  if SearchDirectionForward then
  begin
    TSynEdit(PageControlSourceCode.Page[p].FindComponent(s)).SearchReplace(
      ASearch, '', []);
  end;
end;

function TForm1.ExtractZipResourceIfNotFound(aResourceName: String): Boolean;
var
  S: TResourceStream = nil;
begin
  aResourceName := LowerCase(aResourceName);
  Result := (FileSearch(aResourceName, OpPath, [sfoImplicitCurrentDir])) <> EmptyStr;
  if not Result then
  begin
    S := TResourceStream.Create(HInstance, aResourceName,
          {$IFDEF WINDOWS}Windows.{$ENDIF}
      RT_RCDATA);
    try
      with TFileStream.Create(OpPath + aResourceName, fmCreate) do
        try
          CopyFrom(S, S.Size); // copy data from the resource stream to file stream
        finally
          Free; // destroy the file stream
        end;
    finally
      S.Free; // destroy the resource stream
    end;
  end;
end;

function TForm1.FileExists(const Path: String): Boolean;
var
  Info: TUnicodeSearchRec;
begin
  Result := False;
  Info := Default(TUnicodeSearchRec);
  FillChar(Info, SizeOf(Info), 0); {initialize FindFirst results Info}
  if (FindFirst(Unicodestring(Path), faAnyFile, Info) = 0)  {file was found} and
    ((Info.Attr and faDirectory) <> faDirectory)
  {it's a file, not a directory} then
    Result := True;
  FindClose(Info);
end;

//case SENSITIVE FileExists
function TForm1.FileExistsStr(const aCaseSensitiveFileName: String): Boolean;
var
  FoundFiles: TStringList = nil;
  i: Integer = 0;
  sPath: String = '';
  sName: String = '';
  sFixed: String = '';
begin
  Result := False;
  with TStringList.Create do
    try
      //change directory separator chars to appropriate for the system
      sFixed := ExpandFileName(aCaseSensitiveFileName);
      sPath := ExtractFilePath(sFixed);
      sName := ExtractFileName(sFixed);
      FoundFiles := FindAllFiles(sPath, sName, False);
      //ShowMessage('Number of Files Found= ' + IntToStr(FoundFiles.Count));
      if FoundFiles.Count > 0 then
        for i := 0 to FoundFiles.Count - 1 do
        begin
          if CompareStr(sFixed, FoundFiles[i]) = 0 then Result := True;
          //ShowMessage('sFixed=' + sFixed + lf + 'FoundFiles[' + IntToStr(i) + ']=' + FoundFiles[i] + lf + 'Result= ' + IFThen(Result));
        end;
    finally
      FoundFiles.Free;
    end;
end;


procedure TForm1.FilterEpsOnTitle(DataSet: TDataSet; var Accept: Boolean);
begin
  Accept := True;
  if DataSet.Active then
    with D[iEps] do
    begin
      D[iEps].aFilterDescription :=
        IfThen(aFilterText <> EmptyStr, QuoteChr + aFilterText + QuoteChr, '');
      if aFilterText <> EmptyStr then
        Accept := aFilterText = aDB.FieldByName('Title').AsString;
    end;
end;

procedure TForm1.FilterLogOnUndo(DataSet: TDataSet; var Accept: Boolean);
begin
  D[iLog].aFilterDescription := 'SELECTED';
  Accept := D[iLog].aDB.FieldByName('UnDo').AsBoolean;
end;

procedure TForm1.FilterShowsOnDirInTitle(DataSet: TDataSet; var Accept: Boolean);
begin
  with D[iShows] do
  begin
    aFilterDescription := 'DIRinTITLE';
    if aFilterText = EmptyStr then
      Accept := True
    else
      Accept := ContainsText(D[iShows].aDB.FieldByName('StripDir').AsString,
        aFilterText);
  end;
end;

procedure TForm1.FilterShowsOnTitleDir(DataSet: TDataSet; var Accept: Boolean);
begin
  with D[iShows] do
  begin
    aFilterDescription := IfThen(aFilterText <> EmptyStr, 'SEARCHTHIS[' +
      QuoteChr + aFilterText + Quotechr + ']', '');
    if aFilterText = EmptyStr then
      Accept := True
    else
      //Accept := Pos(aFilterText, DataSet.FieldByName('SearchThis').AsString) > 0;
      Accept := ContainsText(DataSet.FieldByName('SearchThis').AsString, aFilterText);
  end;
end;

procedure TForm1.FindDialogFind(Sender: TObject);
begin
  with Sender as TFindDialog do
    //ShowMessage('ComponentCount= '+IntToStr(Form1.ComponentCount));
    //if FocusedComponent > 0 then
    //begin
    //  ShowMessage('Focused Component= ' + Form1.Components[FocusedComponent].Name);
    //  if Form1.Components[FocusedComponent] is TSynEdit then
    //  begin
    //    ShowMessage('Focused Component= ' + Form1.Components[FocusedComponent].Name);
    //   FindDialog.
    //    FFoundPos := PosEx(FindText, TSynEdit(Components[i]).Lines.Text, FFoundPos+1);
    //      end;
    //  if IsPublishedProp(TControl(ActiveControl), 'Lines') then
    //  begin
    //    FFoundPos := PosEx(FindText, TControl(ActiveControl).Lines.Text, , FFoundPos+1);
    //  end;
    //until ;
    //FFoundPos := PosEx(FindText, Memo1.Lines.Text, FFoundPos+1);
    //if FFoundPos > 0 then
    //begin
    //  Memo1.SelStart := FFoundPos - 1;
    //  Memo1.SelLength := Length(FindText);
    //  Memo1.SetFocus; // Memo1 must be activated, otherwise the selection effect will not be displayed
    //end else
    //  Beep();
  ;
end;

function TForm1.FindMyTitle(sFind: String): String;
var
  IsFound: Boolean = False;
begin
  with D[iShows].aDB, D[iShows] do
  begin
    Result := '';
    if not (DBGridShows.Focused) then
    begin
      if LastKeyTyped = LineEnding then Next;
      aFilterDescription := 'TITLE';
      aFilterText := trim(UpCase(sFind));
      OnFilterRecord := @FilterShowsOnTitleDir;
      Filtered := True;
      //go to first matching record, then re-draw only matches
      FindFirst;
      //found, if only 1 series
      IsFound := ExactRecordCount = 1;
      //remove punctuation, then try again
      if (not IsFound) then
      begin
        aFilterText := RemovePunctuation(aFilterText);
        FindFirst; //go to first matching record, then re-draw only matches
        IsFound := ExactRecordCount = 1; //found, if only 1 series
      end;
      if ExactRecordCount = 1 then
      begin
        IsFound := True;
        D[iList].aDB.Edit;
        D[iList].aDB.FieldByName('Title').AsString :=
          D[iShows].aDB.FieldByName('Title').AsString;
        D[iList].aDB.Post;
      end;
    end;
    if IsFound or IsExit then
      Result := FieldByName('Title').AsString;
    EnableControls;
  end;
end;

//function TForm1.FindPathTo(sFind: String): String;
//var
//  FileIsFound: Boolean = False;
//  i: Integer = 0;
//  SearchList: TStringList = nil;
//  searchme: String = '';
//begin
//  Result := '';
//  sFind := sFind;
//  SearchList := TStringList.Create;
//  SearchList.Delimiter := PathSep;
//  // don't count spaces as delimiters
//  SearchList.StrictDelimiter := True;
//  SearchList.DelimitedText := GetEnvironmentVariable('PATH');
//  SearchList.Insert(0, OpDir);
//  SearchList.Insert(0, '.');
//  i := 0;
//  while (not FileIsFound) and (i < SearchList.Count) do
//  begin
//    searchme := SearchList[i] + PathDelim + sFind;
//    FileIsFound := FileExists(searchme);
//    if FileIsFound then
//      Result := ExcludeTrailingPathDelimiter(searchme);
//    i := i + 1;
//  end;
//  FreeAndNil(SearchList);
//end;


//I like my version better, but let's go with standard FileSearch function
function TForm1.FindPathTo(sFind: String): String;
begin
  Result := ExcludeTrailingPathDelimiter(FileSearch(sFind, OpDir +
    DirSep + PathSeparator + GetEnvironmentVariable('PATH'),
    [sfoImplicitCurrentDir]));
end;

function TForm1.FindTitleByEpName(Sender: TObject): String;
var
  IsFound: Boolean = False;
  sFind: String = '';
  sFindEp: String = '';
begin
  with D[iEps].aDB do
  begin
    DisableControls;
    Result := EmptyStr;
    sFind := StripNonAlphaNum(D[iList].aDB.FieldByName('EpName').AsString);
    StatusBar.Panels[0].Text := 'Searching for Title by Episode Names';
    First;
    //ShowMessage('sFindEp= '+sFindEp+lf+'sFind= '+sFind+lf+'IsFound= '+ifthen(isfound,'true','false'));
    while (not EOF) and (not IsFound) do
    begin
      DownloadEps(Sender);
      D[iEps].aFilterText := trim(D[iList].aDB.FieldByName('Title').AsString);
      //ShowMessage('sFind= '+sFind+lf+'aFilterText= '+D[iEps].aFilterText);
      SetFilterEpsOnTitle;
      D[iList].aDB.EnableControls;
      while not (D[iEps].aDB.EOF) and (not IsFound) do
      begin
        sFindEp := D[iEps].aDB.FieldByName('SortOrder').AsString;
        if not (not ContainsText(sFindEp, 'S01E01PILOT')) then
        begin
          IsFound := ContainsText(sFindEp, sFind);
          if (not IsFound) and (Trim(RemovePunctuation(sFind)) <> EmptyStr) then
            IsFound := ContainsText(RemovePunctuation(sFind),
              RemovePunctuation(sFindEp));
        end;
        //ShowMessage('sFindEp= '+sFindEp+lf+'sFind= '+sFind+lf+'IsFound= '+ifthen(isfound,'true','false'));
        D[iEps].aDB.Next;
      end;
      Next;
    end;
    if IsFound then
      Result := D[iEps].aDB.FieldByName('Title').AsString;
    EnableControls;
  end;
end;

function TForm1.FindYear(sInputname: String): String;
begin
  Result := FindYearExec(sInputname, '(19|20)\d{2}');   //find: 1900-2099
end;

function TForm1.FindYearExec(sInputname, sExpr: String): String;
begin
  Result := EmptyStr;
  SYear := '';
  APieces.Clear;
  //RegEx is global, to avoid time creating/destroying it repeatedly
  RegEx.Expression := sExpr;
  if sInputName <> EmptyStr then
    if RegEx.Exec(sInputName) then
    begin
      sYear := RegEx.Match[0];
      RegEx.Split(UTF8Encode(sInputname), APieces);
      //sTitle is curFile name, up to the Year
      if APieces.Count > -1 then sTitle := APieces[0];
      if APieces.Count > 0 then sEpname := APieces[1];
    end;
  Result := sYear;
end;

function TForm1.FindYearPart(sInputname: String): String;
begin
  Result := FindYearExec(sInputname, '(19|20)\d{0,2}'); //find: 1900-2099
end;

function TForm1.FixChars(s: String): String;
begin
  s := ReplaceText(s, '&amp;', #38);
  s := ReplaceText(s, '&nbsp;', #32);
  s := ReplaceText(s, '&quot;', #22);
  s := ReplaceText(s, '&apos;', #39);
  s := ReplaceText(s, '&iexcl;', #161);
  s := ReplaceText(s, '&#161;', #161);
  s := ReplaceText(s, '&cent;', #162);
  s := ReplaceText(s, '&pound;', #163);
  s := ReplaceText(s, '&#246;', #246);
  s := ReplaceText(s, '&#228;', #228);
  Result := CP437ToUTF8(s);
end;

function TForm1.FixWinFileName(s: String): String;
var
  BadChars: set of Char = ['<', '>', ':', '"', '/', '\', '|', '?', '*', #0..#31, #127];
  i: Integer = 0;
begin
  if length(s) > 0 then
    for i := 1 to length(s) do
      if s[i] in BadChars then
        s[i] := '.';
  Result := ReplaceStr(s, '..', '.');
end;

procedure TForm1.FormActivate(Sender: TObject);
begin
  //if First Run, then width = all buttons at current font size
  if not FileExists(IniFileName) then
    Form1.Width := ToolBarWidthSize(ToolBarFiles);
  //remove Wine extra .lnk file
  if GetEnv('WINEPREFIX') <> EmptyStr then
    CreateDesktopShortcut;
  DownloadShowsIfEmpty;
end;

procedure TForm1.FormClose(Sender: TObject; var CloseAction: TCloseAction);
var
  i: Integer = 0;
  j: Integer = 0;
begin
  if not IsIniFileDeleted then
    with IniFile, StatusBar.Canvas do
    begin
      WriteBool('Main', 'IsCreateDesktopShortcut', IsCreateDesktopShortcut);
      WriteInteger('Main', 'Height', Form1.Height);
      WriteInteger('Main', 'Left', Form1.Left);
      WriteInteger('Main', 'Top', Form1.Top);
      WriteInteger('Main', 'Width', Form1.Width);
      //WriteString('Main', 'Bounds', BoundsToDelimited(Form1.BoundsRect));
      WriteString('Main', 'Form1.Canvas.Font.Name', Form1.Canvas.Font.Name);
      WriteInteger('Main', 'Form1.Canvas.Font.Size', Form1.Canvas.Font.Size);
      WriteBool('Main', 'Form1.Canvas.Font.Bold', Form1.Canvas.Font.Bold);
      WriteString('Main', 'StatusBar.Canvas.Font.Name', Font.Name);
      WriteInteger('Main', 'StatusBar.Canvas.Font.Size', Font.Size);
      WriteBool('Main', 'StatusBar.Canvas.Font.Bold', Font.Bold);
      //WriteBool('Main', 'IsHideRenamedCount', Form1.IsHideRenamedCount);

      if PageControlSourceCode.PageCount > 0 then
      begin
        WriteInteger('Main', 'PageControlSourceCode.PageCount',
          PageControlSourceCode.PageCount);
        for i := 0 to PageControlSourceCode.PageCount - 1 do
          with PageControlSourceCode.Pages[i] do
            if ComponentCount > 0 then
              for j := 0 to ComponentCount - 1 do
                if Components[j] is TSynEdit then
                begin
                  WriteString('Main', 'TabSheetSourceCode' + i.ToString + '.Font.Name',
                    TSynEdit(Components[j]).Font.Name);
                  WriteInteger('Main', 'TabSheetSourceCode' + i.ToString + '.Font.Size',
                    TSynEdit(Components[j]).Font.Size);
                end;
      end;
    end;
  for i := Ord(Low(DBFs)) to Ord(High(DBFs)) do
    //must CLOSE before Free, prevent Storage:= stoMemory memory leak
    if Assigned(D[i].aDB) then
      D[i].aDB.Close;
  if Assigned(IniFile) then IniFile.Free;
  if Assigned(APieces) then APieces.Free;
  if Assigned(RegEx) then RegEx.Free;
  if Assigned(MyObject) then MyObject.Free;
  if Assigned(MyComponent) then MyComponent.Free;
  IsExit := True;
  CloseAction := caFree;
end;

procedure TForm1.FormCreate(Sender: TObject);
var
  i: Integer = 0;
  j: Integer = 0;
  s: String = '';
  MonoFonts: TStringList;
const
  Borderless_Frame_Style: Longword =
    WS_POPUP or WS_THICKFRAME or WS_MINIMIZEBOX or WS_MAXIMIZEBOX or WS_CAPTION;
begin
  MyComponent := TComponent.Create(Application);
  MyObject := TObject.Create;
  ShowInTaskBar := stAlways;
  BorderIcons := [biMaximize, biMinimize, biSystemMenu];
  Anchors := [];
  Align := alNone;
  DragMode := dmAutomatic;
  DragKind := dkDrag;
  IsIniFileDeleted := False;
  //ensure Minimize button in Windows
  if IsWindows then
    SetWindowLong(self.handle, GWL_STYLE, Borderless_Frame_Style);
  s := trim(OSVersion);
  IsWindows := leftstr(s, 7) = 'Windows';
  IsLinux := leftstr(s, 5) = 'Linux';
  IsWine := GetEnv('WINEPREFIX') <> EmptyStr;
  APieces := TStringList.Create;
  buttonSelected := 0;
  Caption := Application.Title;
  CopiedCount := 0;
  if IsWindows then OpDir :=
      GetEnvironmentVariableUTF8('AppData') + PathDelim + 'filey'
  else
  if IsLinux then OpDir := GetEnvironmentVariableUTF8('HOME') + '/.local/share/filey'
  else
    OpDir := TrimFilename(ExtractFileDir(Application.ExeName));
  ProgramPath := ExtractFilePath(Application.ExeName);
  ForceDirectories(OpDir);
  OpPath := OpDir + PathDelim;
  //ConfigFileDir := GetAppConfigDir(False);
  ConfigFileDir := OpDir;
  FoundTitle := EmptyStr;
  IniFileName := ConfigFileDir + PathDelim + ExtractFileNameOnly(
    Application.ExeName) + '.ini';
  actDeleteIniFile.Caption := 'Delete ' + IniFileName + ' configuration file';
  IniFile := TIniFile.Create(IniFileName);
  IsInternetConnected := HasInternetConnection;
  IsModalWait := False;
  LastTitleFound := EmptyStr;
  PageControlAbout.ActivePageIndex := TabSheetInfo.PageIndex;
  PageControlMain.ActivePageIndex := TabSheetList.PageIndex;
  RegEx := TRegExpr.Create;
  sEpName := EmptyStr;
  sEpNum := EmptyStr;
  sFrom := EmptyStr;
  sFromPath := EmptyStr;
  SkipCount := 0;
  SelectedTitle := EmptyStr;
  SourcePath := ExpandFileName(ExtractFileDir(Application.ExeName) +
    DirSep + '..' + DirSep + '..') + DirSep;
  SourceFilesCount := 0;
  sTitle := EmptyStr;
  sTo := EmptyStr;
  sToday := FormatDateTime('YYYY/MM/DD', Today);
  sToPath := EmptyStr;
  sYear := EmptyStr;
  WasSelected := False;

  MonoFonts := TStringList.Create;
  MonoFonts.Delimiter := ',';
  MonoFonts.DelimitedText := 'Consolas,Monospace,Mono,FreeMono,Courier New,Courier';
  for i := 0 to MonoFonts.Count - 1 do
  begin
    for j := 0 to Screen.Fonts.Count - 1 do
      if trim(Screen.Fonts[j]) = trim(MonoFonts[i]) then
      begin
        StatusBar.Canvas.Font.Name := trim(Screen.Fonts[j]);
        StatusBar.Canvas.Font.Bold := True;
        break;
      end;
    if trim(Screen.Fonts[j]) = trim(MonoFonts[i]) then break;
  end;
  MonoFonts.Free;
  FontDialog.MinFontSize := 6;
  FontDialog.MaxFontSize := 48;
  FontDialog.Options := [fdForceFontExist, fdLimitSize];

  with IniFile do
  begin
    if FileExists(IniFileName) then
      Form1.Position := poDesigned
    else
      Form1.Position := poScreenCenter;
    Form1.AutoSize := False; //Must be FALSE to restore window bounds
    SetBounds(Form1, ReadInteger('Main', 'Left', 530),
      ReadInteger('Main', 'Top', 200), ReadInteger('Main', 'Width', 846),
      ReadInteger('Main', 'Height', 570));
    Form1.Canvas.Font.Name :=
      ReadString('Main', 'Form1.Canvas.Font.Name', 'Serif');
    Form1.Canvas.Font.Size :=
      ReadInteger('Main', 'Form1.Canvas.Font.Size', 14);
    Form1.Canvas.Font.Bold :=
      ReadBool('Main', 'Form1.Canvas.Font.Bold', False);
    StatusBar.Canvas.Font.Name :=
      ReadString('Main', 'StatusBar.Canvas.Font.Name', StatusBar.Canvas.Font.Name);
    StatusBar.Canvas.Font.Size :=
      ReadInteger('Main', 'StatusBar.Canvas.Font.Size', 13);
    StatusBar.Canvas.Font.Bold :=
      ReadBool('Main', 'StatusBar.Canvas.Font.Bold', True);
    //IsHideRenamedCount := ReadBool('Main', 'IsHideRenamedCount', False);
    IsHideRenamedCount := False;
    IsHideSelectAShow := False;
    CheckResourceFiles;
    for i := 0 to PageControlSourceCode.PageCount - 1 do
      with PageControlSourceCode.Pages[i] do
        if ComponentCount > 0 then
          for j := 0 to ComponentCount - 1 do
            if Components[j] is TSynEdit then
            begin
              TSynEdit(Components[j]).Font.Name :=
                ReadString('Main', 'TabSheetSourceCode' + i.ToString +
                '.Font.Name', StatusBar.Canvas.Font.Name);
              TSynEdit(Components[j]).Font.Size :=
                ReadInteger('Main', 'TabSheetSourceCode' + i.ToString +
                '.Font.Size', 13);
            end;
    IsCreateDesktopShortcut :=
      ReadBool('Main', 'IsCreateDesktopShortcut', True);
    CreateDesktopShortcut;
    if GetEnv('USER') <> 'mark' then actZipSources.Visible := False;
    //files won't exist for non-developers
  end;

  for i := 0 to Form1.ComponentCount - 1 do
  begin
    if Components[i] is TControl then
    begin
      TControl(Components[i]).AddHandlerOnMouseWheel(
        @MouseWheelScaleToFont, True);
      TControl(Components[i]).AddHandlerOnKeyDown(@TestKeyDown);
      if not (TComponent(Form1.Components[i]) is TSynEdit) then
        TControl(Components[i]).Font := Form1.Canvas.Font;
      TControl(Components[i]).AutoSize := True;
    end;
    if Components[i] is TWinControl then
      TWinControl(Components[i]).DoubleBuffered := True;
    if IsPublishedProp(Form1.Components[i], 'AutoSize') then
      SetPropValue(Form1.Components[i], 'AutoSize', False);
    if IsPublishedProp(Form1.Components[i], 'ScreenSnap') then
      SetPropValue(Form1.Components[i], 'ScreenSnap', False);

    case Components[i].ClassName of
      'TBitBtn': TBitBtn(Components[i]).AutoSize := True;
      'TDBGrid':
      begin
        TDBGrid(Components[i]).OnDblClick := @DBGridDblClick;
        TDBGrid(Components[i]).OnMouseMove := @DBGridMouseMove;
        TDBGrid(Components[i]).OnTitleClick := @DBGridTitleClick;
      end;
      'TPanel': TPanel(Components[i]).AutoSize := True;
      'TSynEdit': TSynEdit(Components[i]).Lines.Clear;
      'TToolBar': TToolBar(Components[i]).AutoSize := True;
    end; {case}
  end;

  EpGuidesURL := 'https://www.epguides.com/';
  sURL := EpGuidesURL;
  IpHtmlPanelBrowser.DataProvider := nil;
  AllShowsTxt := 'allshows.txt';
  actHintsExecute(Form1); //turn on all hints
  StatusBar.Panels[0].Style := psOwnerDraw;
  StatusBar.Panels[0].Width := StatusBar.Width - 2;
  PanelSelect.Visible := False;
  //D[iShows].aFilterText := EmptyStr;
  RegisterTest('Test Databases', TTestDatabases);
  RegisterTest(TMyTestCase);
  DBGridList.OnMouseDown := @RightMouseFonts;
  DBGridLog.OnMouseDown := @RightMouseFonts;
  InitializeDBFs;
end;



procedure TForm1.FormDestroy(Sender: TObject);
var
  i: Integer = 0;
begin
  //probably unnecessary, but belt & suspenders
  for i := Form1.ComponentCount - 1 downto 0 do
    Form1.Components[i].Free;
end;

procedure TForm1.FormDropFiles(Sender: TObject; const FileNames: array of String);
var
  Files: TStringList = nil;
  i: Integer = 0;
begin
  Files := TStringList.Create;
  for i := 0 to High(FileNames) do
    Files.Add(Filenames[i]);
  if Files.Count > 0 then
    AddFiles(Files);
  FreeAndNil(Files);
end;

procedure TForm1.FormResize(Sender: TObject);
begin
  Form1.DisableAutoSizing;
  DBGridListSetColumnWidths;
  StatusBar.Panels[0].Width := StatusBar.Width - 2;
  Form1.EnableAutoSizing;
  IsFormSnapped := (Form1.Width = Screen.WorkAreaWidth div 2) or
    (Form1.Width = Screen.WorkAreaWidth);
end;

procedure TForm1.FormShow(Sender: TObject);
begin
  DownloadShowsIfEmpty;
end;

procedure TForm1.FormShowHint(Sender: TObject; HintInfo: PHintInfo);
var
  C: Integer = 0;
  R: Integer = 0;
begin
  //ShowMessage(HintInfo^.HintControl.Name);
  if (HintInfo^.HintControl = DBGridLog) and (not Form1.IsResizing) then
  begin
    Form1.DisableAlign;
    R := 0;
    C := 0;
    DBGridLog.MouseToCell(HintInfo^.CursorPos.X, HintInfo^.CursorPos.Y, C, R);
    HintInfo^.CursorRect := DBGridLog.CellRect(C, R);
    HintInfo^.HintStr := 'R= ' + IntToStr(r) + #13 + 'C=' + IntToStr(c);
    DBGridLog.Hint := HintInfo^.HintStr;
    Form1.EnableAlign;
  end;
end;

function TForm1.GetDBFStatus(aDB: TDbf): String;
var
  i: Integer = 0;
  s: String = '';
begin
  with aDB do
  begin
    s := s + 'Name= ' + aDB.Name + LF;
    s := s + 'FilePathFull= ' + aDB.FilePathFull + LF;
    s := s + 'TableName= ' + aDB.TableName + LF;
    s := s + 'Active= ' + IfThen(aDB.Active) + LF;
    s := s + 'ReadOnly= ' + IfThen(ReadOnly) + LF;
    s := s + 'Exclusive= ' + IfThen(Exclusive) + LF;
    s := s + 'ControlsDisabled= ' + IfThen(ControlsDisabled) + LF;
    if IndexFieldNames <> EmptyStr then
      s := s + 'IndexFieldNames= ' + IndexFieldNames + LF;
    IndexDefs.Update;
    if IndexDefs.Count > 0 then
      for i := 0 to IndexDefs.Count - 1 do
      begin
        s := s + 'Index[' + IntToStr(i) + '].Name= ' + IndexDefs.Items[i].Name + LF;
        s := s + 'Index[' + IntToStr(i) + '].Expression= ' +
          IndexDefs.Items[i].Expression + LF;
      end;
    if Filter <> EmptyStr then
      s := s + 'Filter= ' + aDB.Filter + LF;
    s := s + 'PhysicalRecCount= ' + IntToStr(PhysicalRecordCount);
    Result := s;
  end;
end;

function TForm1.GetDeepestDir(const aFilename: String): String;
begin
  Result := ExtractFileName(ExtractFileDir(aFilename));
end;


function TForm1.GetEpName(const curFile: String): String;
var
  curFileNameOnly: String = '';
begin
  with D[iList].aDB do
  begin
    Edit;
    curFileNameOnly := ExtractFileNameOnly(curFile);
    Result := '';
    aPieces.Clear; //EXTREMELY important to get correct titles after 1st line
    RegEx.Split(UTF8Encode(curFileNameOnly), APieces);
    if APieces.Count > 1 then
    begin
      FieldByName('Title').AsString := TrimPeriod(APieces[0]);
      FieldByName('EpName').AsString := TrimPeriod(APieces[1]);
      Result := TrimPeriod(APieces[1]);
    end
    else
      FieldByName('Title').AsString := ExtractFileNameOnly(curFile);
    PanelFilesGrid.ShowHint := True;
    PostRecord(D[iList].aDB);
  end;
end;

function TForm1.GetEpNum(const curFile: String): String;
var
  i: Integer = 0;
  s: String = '';
  sEpNo: String = '';
const
  RegExExpressionMax = 12;
  sRegExExpression: array[0..RegExExpressionMax] of String =
    ('[Ss]\d{2}[Ee]\d{2}',     //find: S01E01
    '[Ss]\d{2}.[Ee]\d{2}',     //find: S01.E01
    '[Ss]\d{2}[Ee]\d{1}',     //find: S01E1
    '[Ss]\d{2}.[Ee]\d{1}',     //find: S01.E1
    '[Ss]\d{1}[Ee]\d{2}',     //find: S1E01
    '[Ss]\d{1}.[Ee]\d{2}',     //find: S1.E01
    '[Ss]\d{1}[Ee]\d{1}',     //find: S1E1
    '\d{2}[Xx]\d{2}',   //find: '01x01'
    '\d{1}[Xx]\d{2}',   //find: '1x01'
    '\d{1}[Xx]\d{1}',   //find: '1x1'
    '\d{2}[Xx]\d{1}',   //find: '01x1'
    '\d{1}\d{1}\d{1}\d{1}',   //find: '1001'
    '\d{1}\d{1}\d{1}');   //find: '101'
begin
  //would welcome a more elegant way to parse Episode numbers
  Result := '';
  while (i <= RegExExpressionMax) and (sEpNo = EmptyStr) do
  begin
    RegEx.Expression := sRegExExpression[i];
    //RegEx.Exec(UTF8Decode(curFile));
    RegEx.Exec(curFile);
    s := RegEx.Match[0];
    if s <> EmptyStr then
      case i of
        0: sEpNo := 'S' + copy(s, 2, 2) + 'E' + copy(s, 5, 2);   //found: S01E01
        1: sEpNo := 'S' + copy(s, 2, 2) + 'E' + copy(s, 6, 2);   //found: S01.E01
        2: sEpNo := 'S' + copy(s, 2, 2) + 'E' + copy(s, 5, 1);   //found: S01E1
        3: sEpNo := 'S' + copy(s, 2, 2) + 'E' + copy(s, 6, 1);   //found: S01.E1
        4: sEpNo := 'S0' + copy(s, 2, 1) + 'E' + copy(s, 4, 2);  //found: S1E01
        5: sEpNo := 'S0' + copy(s, 2, 1) + 'E' + copy(s, 5, 2);  //found: S1.E01
        6: sEpNo := 'S0' + copy(s, 2, 1) + 'E0' + copy(s, 4, 1); //found: S1E1
        7: sEpNo := 'S' + copy(s, 1, 2) + 'E' + copy(s, 4, 2);   //found: '01x01'
        8: sEpNo := 'S0' + copy(s, 1, 1) + 'E' + copy(s, 3, 2);  //found: '1x01'
        9: sEpNo := 'S0' + copy(s, 1, 1) + 'E0' + copy(s, 3, 1); //found: '1x1'
        10: sEpNo := 'S' + copy(s, 1, 2) + 'E' + copy(s, 4, 1);   //found: '01x1'
        11: sEpNo := 'S' + copy(s, 1, 2) + 'E' + copy(s, 3, 2);   //found: '1001'
        12: sEpNo := 'S0' + copy(s, 1, 1) + 'E' + copy(s, 2, 2);  //found: '101'
        else
          sEpNo := EmptyStr
      end;
    Inc(i);
  end;
  if sEpNo <> EmptyStr then
  begin
    APieces.Clear;
    RegEx.Split(UTF8Encode(curFile), APieces);
    sEpName := APieces[1]; //text after EpNum is usually EpName, if any
    Result := sEpNo;
  end;
end;

procedure TForm1.GetMainAttributes(curFile: String);
var
  checkdup: String = '';
  s: String = '';
begin
  s := ProbeFile(curFile);
  with D[iList].aDB do
  begin
    //checkdup prevents re-adding info that's already there
    checkdup := FieldByName('TO').AsString;
    FieldByName('Codec').AsString := UpCase(ExtractAfterStr('codec_name=', s));
    if ContainsText(FieldByName('Codec').AsString, '264') then
      FieldByName('Codec').AsString := 'x264';
    if ContainsText(checkdup, FieldByName('Codec').AsString) then
      FieldByName('Codec').AsString := '';
    FieldByName('Height').AsString := ExtractAfterStr('height=', s);
    if ContainsText(checkdup, FieldByName('Height').AsString) then
      FieldByName('Height').AsString := '';

    FieldByName('AudioChan').AsString := ExtractAfterStr('channels=', s);
    if FieldByName('AudioChan').AsString = '2' then
      FieldByName('AudioChan').AsString := '';
    if FieldByName('AudioChan').AsString = '6' then
      FieldByName('AudioChan').AsString := '.DD5.1';
    if ContainsText(s, 'codec_name=eac3') or ContainsText(s, 'E-AC-3,') then
      FieldByName('AudioChan').AsString := 'EAC3' + FieldByName('AudioChan').AsString;
    if ContainsText(s, 'codec_name=mp3') then
      FieldByName('AudioChan').AsString := 'MP3';
    if ContainsText(checkdup, FieldByName('AudioChan').AsString) then
      FieldByName('AudioChan').AsString := '';
    FieldByName('Subtitles').AsString :=
      IfThen(ContainsText(s, '=SubRip ') or (ContainsText(s, '=subtitle')),
      'Subs', '');
    if ContainsText(checkdup, FieldByName('Subtitles').AsString) then
      FieldByName('Subtitles').AsString := '';
  end;
end;

function TForm1.HackFrom(sFind: String; var s, sLeft, sRight: String): String;
var
  k: Integer = 0;
begin
  //sloppy routine to scrape info out of web pages
  k := Pos(sFind, s);
  if k = 0 then
    Result := ''
  else
  begin
    sRight := RightStr(s, length(s) - k - length(sFind) + 1);
    sLeft := sLeft + LeftStr(s, k);
    s := sRight;
    Result := s;
  end;
end;

function TForm1.HackTo(sFind: String; var s, sLeft, sRight: String): String;
var
  k: Integer = 0;
begin
  //more sloppy routine to scrape info out of web pages
  k := Pos(sFind, s);
  if k = 0 then
    Result := ''
  else
  begin
    sRight := RightStr(s, length(s) - k - length(sFind) + 1);
    sLeft := LeftStr(s, k - 1);
    sLeft := ReplaceStr(sLeft, '&nbsp;', '');
    s := sRight;
    Result := sLeft;
  end;
end;

procedure TForm1.IncreaseAllFonts(Amount: Integer);
var
  k: Integer = 0;
  v: Integer = 0;
begin
  //let's not go smaller than 8-point font
  v := Max(Form1.Canvas.Font.Size + Amount, 8);
  for k := 0 to Form1.ComponentCount - 1 do
  begin
    if (Components[k] is TControl) then
      TControl(Components[k]).Font.Size := v;
    if Components[k] is TDBGrid then
      if TDBGrid(Components[k]).Showing then
        TDBGrid(Components[k]).AutoAdjustColumns;
  end;
  StatusBar.Canvas.Font.Size := v - 1;
  Form1.Canvas.Font.Size := v;
  Form1.Width := ToolBarWidthSize(ToolBarFiles);
end;

procedure TForm1.InitializeDBFs;
var
  i: Integer = 0;
begin
  //array D of DBFs allows easy/consistent DBF creation/iteration
  SetAllCursors(crHourGlass);
  for i := Ord(Low(DBFs)) to Ord(High(DBFs)) do
    with D[i] do
      if not Assigned(aDB) then
      begin
        aDB := TDbf.Create(Form1);
        with aDB do
        begin
          //get the name of the DBF from the enumerated type DBFs
          aDB.Name := GetEnumName(TypeInfo(DBFs), Ord(i));
          aDataSource := TDataSource.Create(Form1);
          aFilterDescription := EmptyStr;
          FilePathFull := OpPath;
          FilePath := OpPath;
          TableName := aDB.Name + '.DBF';
          if not FileExists(OpPath + TableName) then
          begin
            Active := False;
            DisableControls;
            Exclusive := True;
            Filter := '';
            Filtered := False;
            FilterOptions := [];
            OpenMode := omAutoCreate;  //omNormal;
            ReadOnly := False;
            ShowDeleted := False;
            //stoMemory requires backingstream:=TMemoryStream.Create; UserStream:=backingstream;
            //stoMemory requires CLOSE dbf before closing program, to prevent memory leak
            if Pos('|' + Name, '|ABOUT, |LIST, |TEMPEPS') > 0 then
            begin
              Storage := stoMemory;
              FilePath := EmptyStr;
              FilePathFull := EmptyStr;
            end
            else
              Storage := stoFile;
            StoreDefs := True;
            TableLevel := 7;
            Tag := i;
            Version := '7.0';
            case aDB.Name of
              'ABOUT':
              begin
                FieldDefs.Add('Info', ftString, 100, True);
                FieldDefs.Add('Value', ftString, 100, True);
              end;
              'EPS', 'TEMPEPS':
              begin
                FieldDefs.Add('Title', ftString, 75, True);
                FieldDefs.Add('EpNum', ftString, 10, True);
                FieldDefs.Add('EpName', ftString, 70, True);
                FieldDefs.Add('SortOrder', ftString, 99, True);
              end;
              'LIST':
              begin
                FieldDefs.Add('Directory', ftString, 60, True);
                FieldDefs.Add('Title', ftString, 75, True);  //Series Name
                FieldDefs.Add('From', ftString, 255, True);
                FieldDefs.Add('FileName', ftString, 100, True);
                FieldDefs.Add('FromPath', ftString, 100, True);
                FieldDefs.Add('NameNPath', ftString, 120, True);
                FieldDefs.Add('EpNum', ftString, 10, True);
                FieldDefs.Add('EpName', ftString, 100, True);
                FieldDefs.Add('Height', ftString, 10, True);
                FieldDefs.Add('AudioChan', ftString, 10, True);
                FieldDefs.Add('Codec', ftString, 10, True);
                FieldDefs.Add('Group', ftString, 30, True);
                FieldDefs.Add('Extension', ftString, 10, True);
                FieldDefs.Add('Subtitles', ftString, 10, True);
                FieldDefs.Add('To', ftString, 255, True);
                FieldDefs.Add('ToPath', ftString, 100, True);
                FieldDefs.Add('TreeLevel', ftString, 1, True);
              end;
              'LOG':
              begin
                FieldDefs.Add('UNDO', ftBoolean, 1, True);
                FieldDefs.Add('DATETIME', ftString, 16, True);
                FieldDefs.Add('FROMPATH', ftString, 255, True);
                FieldDefs.Add('FROM', ftString, 255, True);
                FieldDefs.Add('TOPATH', ftString, 255, True);
                FieldDefs.Add('TO', ftString, 255, True);
              end;
              'SHOWS':
              begin
                FieldDefs.Add('Title', ftString, 75, True);
                FieldDefs.Add('Directory', ftString, 60, True);
                FieldDefs.Add('TVRage', ftString, 10, True);
                FieldDefs.Add('TVMaze', ftString, 10, True);
                FieldDefs.Add('Start_Date', ftString, 20, True);
                FieldDefs.Add('End_Date', ftString, 70, True);
                FieldDefs.Add('Num_Eps', ftString, 75, True);
                FieldDefs.Add('Run_Time', ftString, 50, True);
                FieldDefs.Add('Network', ftString, 40, True);
                FieldDefs.Add('Country', ftString, 10, True);
                FieldDefs.Add('OnHiatus', ftBoolean, 2, True);
                FieldDefs.Add('HiatusDesc', ftString, 30, True);
                FieldDefs.Add('LookUpDate', ftString, 10, True);
                //for searching/matching
                FieldDefs.Add('SearchThis', ftString, 100, True);
              end;
              else
                NoOp
            end;
            CreateTable;
            EnableControls;
          end;
          AssignDBFDataSource(aDB, aDataSource);
          AfterScroll := @DBGridListAfterScroll;   // must be inactive
          BeforeScroll := @DBGridListBeforeScroll;
          Exclusive := True;
          Open;
        end;
      end; //not assigned{with D[i]}
  SetupDbShows;
  SetupDbEps;
  SetupDbList;
  //SetupDbLog; //don't load until user clicks on it
  //SetupDbAbout;  //don't load until user clicks on it
  SetAllCursors(crDefault);
end;

procedure TForm1.IpHtmlPanelBrowserDocumentOpen(Sender: TObject);
begin
  if Assigned(edUrl) and Assigned(IpHtmlPanelBrowser) then
    try
      edUrl.Text := IpHtmlPanelBrowser.CurURL;
    except
      edUrl.Text := EmptyStr;
    end;
end;

procedure TForm1.LoadDbfFromCSVFile(var aDB: TDbf; AFilename: String;
  ADelimiter: Char = ','; WithHeader: Boolean = True; AddRows: Boolean = True);

  function IsBlankRow: Boolean;
  var
    i: Integer = 0;
    s: String = '';
  begin
    Application.ProcessMessages;
    for i := 0 to aDB.FieldCount - 1 do
      if aDB.Fields[i].FieldDef.DataType = ftString then
        s := s + aDB.Fields[i].AsString;
    Result := (length(trim(s)) = 0);
  end;

var
  FileStream: TFileStream = nil;
  Parser: TCSVParser = nil;
  PForm: TProgressForm = nil;
begin
  FileStream := Default(TFileStream);
  Parser := Default(TCSVParser);
  PForm := Default(TProgressForm);
  AddRows := AddRows;
  aDB.DisableControls;
  ZapPackTable(aDB);
  aDB.Exclusive := True;
  aDB.Append;
  PForm := TProgressForm.Create(Application);
  if FileExists(AFileName) then
  begin
    PageControlMain.ActivePageIndex := TabSheetShows.PageIndex;
    DBGridShows.Enabled := False;
    Parser := TCSVParser.Create;
    FileStream := TFileStream.Create(AFilename, fmOpenRead + fmShareDenyWrite);
    PForm.ProgressBar1.Max := FileStream.Size;
    PForm.Caption := 'Getting TV show names';
    PForm.Label1.Caption := EmptyStr;
    PForm.Label1.Font.Style := [fsBold];
    PForm.IsClicked := False;
    PForm.Enabled := True;
    PForm.BitBtn.Kind := bkCancel;
    PForm.Show;
    Parser.Delimiter := ADelimiter;
    Parser.SetSource(FileStream);
    while (Parser.ParseNextCell) and (not PForm.IsClicked) do //ParseNextCell
      with aDB do
      begin
        if (WithHeader) and (Parser.CurrentRow = 0) then
          aDB.Fields[Parser.CurrentCol].Fieldname := Parser.CurrentCellText
        else
        begin
          Exclusive := True;
          if Parser.CurrentCol = 0 then
            if IsBlankRow and (aDB.PhysicalRecordCount > 0) then
              Delete
            else
              Append;
          case Fields[Parser.CurrentCol].FieldDef.DataType of
            ftString: Fields[Parser.CurrentCol].AsString :=
                FixChars(Parser.CurrentCellText);
            ftBoolean: Fields[Parser.CurrentCol].AsBoolean :=
                ContainsText(Parser.CurrentCellText, 'true');
            else
              Fields[Parser.CurrentCol].AsString := '';
          end; {case}
          //pre-stripping Titles and Directories makes filter-matching much faster
          FieldByName('SearchThis').AsString :=
            trim(FieldByName('Title').AsString) + ' ' +
            trim(FieldByName('Directory').AsString) + ' ' +
            RemovePunctuation(FieldByName('Title').AsString) + ' ' +
            RemovePunctuation(FieldByName('Directory').AsString);
          FieldByName('SearchThis').AsString :=
            UpCase(FieldByName('SearchThis').AsString);
          FieldByName('LookUpDate').AsString := EmptyStr;
        end; {not header row}
        PForm.ProgressBar1.Position := FileStream.Position;
        PForm.Label1.Caption :=
          IntToStr(FileStream.Position) + '/' + IntToStr(FileStream.Size) + ' fields';
        PForm.Label1.Left := (PForm.PanelProgress.Width - PForm.Label1.Width) div 2;
        //occassionally update DBFShows grid on screen
        //Optimization: If second operand of a mod on an unsigned value is
        //power of 2, then fast AND instruction is used instead of slow integer division.
        if FileStream.Position mod 64 = 0 then
        begin
          aDB.EnableControls;
          if FileStream.Position mod 8192 = 0 then
            DBGridShows.AutoAdjustColumns;
          aDB.DisableControls;
        end;
      end; {ParseNextCell}
    if not PForm.IsClicked then
      if not IsBlankRow then
        PostRecord(aDB);
  end; {fileexists}
  PackThisTable(aDB);
  aDB.IndexDefs.Clear; //trigger re-index, later
  aDB.IndexDefs.Update;
  aDB.AddIndex('TITLE', 'TITLE', [ixCaseInsensitive, ixUnique]);
  aDB.IndexName := 'TITLE';
  aDB.RegenerateIndexes;
  FreeAndNil(FileStream);
  FreeAndNil(Parser);
  PForm.Hide;
  FreeAndNil(PForm);
  MenuShowsToggleHiddenClick(self);
  MenuShowsToggleHiddenClick(self);
  Form1.Show;
end;

procedure TForm1.LogAfterPost(DataSet: TDataSet);
begin
  SetBitBtnUndoEnabled;
end;

procedure TForm1.LogAfterScroll(DataSet: TDataSet);
begin
  SetRefreshButton(DBNavigatorLog);
  ShowStatusDBF(TDbf(DataSet), D[iLog].aFilterDescription);
end;

procedure TForm1.LogBeforeEdit(DataSet: TDataSet);
begin
  //BitBtnUndo.Enabled := True;
  NoOp;
end;

procedure TForm1.LogBeforeScroll(DataSet: TDataSet);
begin
  ShowStatusDBF(TDbf(DataSet), D[iLog].aFilterDescription);
end;

procedure TForm1.MenuItemDeleteShowClick(Sender: TObject);
begin
  DBNavigatorShows.BtnClick(nbDelete);
end;

procedure TForm1.MenuItemInsertClick(Sender: TObject);
begin
  if MessageDlg('Confirm Insert', 'Confirm Insert', mtConfirmation, [mbYes, mbNo], 0) =
    mrYes then DBNavigatorShows.BtnClick(nbInsert);
end;

procedure TForm1.MenuShowEnvClick(Sender: TObject);
var
  i: Integer = 0;
  s: String = '';
begin
  for i := 0 to Dos.EnvCount do
  begin
    s := s + Dos.EnvStr(i) + LF;
    if (i > 0) and (i mod 32 = 0) then
    begin
      MessageDlgEx('Environment Variables:' + LineEnding + LineEnding + s,
        mtConfirmation, [mbOK], Form1);
      s := EmptyStr;
    end;
  end;
  MessageDlgEx('Environment Variables:' + LineEnding + LineEnding +
    s, mtConfirmation, [mbOK], Form1);
end;

procedure TForm1.MenuShowsToggleHiddenClick(Sender: TObject);
var
  i: Integer = 0;
begin
  with DBGridShows do
    for i := 3 to Columns.Count - 1 do
      Columns[i].Visible := not Columns[i].Visible;
  DBGridShows.Visible := True;
  DBGridShows.Enabled := True;
  DBGridShows.AutoFillColumns := not DBGridShows.AutoFillColumns;
  D[iShows].aDB.IndexName := IfThen(DBGridShows.Columns[3].Visible, '', 'TITLE');
  D[iShows].aDB.EnableControls;
  D[iShows].aDB.Active := True;
  DBGridShows.AutoAdjustColumns;
  UpdateStatusBar;
end;

function TForm1.MessageDlgEx(const AMsg: String; ADlgType: TMsgDlgType;
  AButtons: TMsgDlgButtons; AParent: TForm): TModalResult;
var
  MsgFrm: TForm = nil;
begin
  MsgFrm := CreateMessageDialog(AMsg, ADlgType, AButtons);
  try
    MsgFrm.Position := poDefaultSizeOnly;
    MsgFrm.FormStyle := fsSystemStayOnTop;
    MsgFrm.Left := AParent.Left + (AParent.Width - MsgFrm.Width) div 2;
    MsgFrm.Top := AParent.Top + (AParent.Height - MsgFrm.Height) div 2;
    Result := MsgFrm.ShowModal;
  finally
    MsgFrm.Free
  end;
end;

procedure TForm1.MessageList(title: String; sl: TStringList);
var
  i: Integer = 0;
  s: String = '';
begin
  s := title + ':' + LineEnding;
  for i := 0 to sl.Count - 1 do
    s := s + sl[i] + LineEnding;
  ShowMessage(s);
end;

procedure TForm1.MessageRenamedCount(const buttonSelected: Integer);
var
  s: String = '';
begin
  if not IsHideRenamedCount then
  begin
    if (CopiedCount > 0) then
      s := 'Renamed ' + IntToStr(CopiedCount) + IfThen(
        (CopiedCount = 1), ' file.', ' files.');
    if (CopiedCount = 0) and (buttonSelected <> mrCancel) then
      s := 'Files NOT renamed.' + LineEnding;
    if (CopiedCount = 0) and (buttonSelected = mrCancel) then
      s := 'Canceled.' + LineEnding;
    s := s + LineEnding + IfThen(SkipCount > 0, ('Skipped ' +
      IntToStr(SkipCount) + ' file' + IfThen((SkipCount = 1), '.', 's.')));
    IsHideRenamedCount := ShowMessage(s, IsHideRenamedCount);
  end;
end;

procedure TForm1.MouseWheelScaleToFont(Sender: TObject; Shift: TShiftState;
  WheelDelta: Integer; MousePos: TPoint; var Handled: Boolean);
begin
  MousePos := MousePos;
  Handled := Handled;
  if (IsLinux and (ssCtrl in Shift)) or (IsWindows and
    ([ssCtrl] * Shift = [ssCtrl])) then
    if WheelDelta > 0 then
      IncreaseAllFonts(1)
    else
      IncreaseAllFonts(-1);
end;

function TForm1.OSVersion: String;
begin
  {$IFDEF LCLcarbon}
  Result := 'Mac OS X 10.';
  {$ELSE}
  {$IFDEF Linux}
  Result := 'Linux Kernel ';
  {$ELSE}
  {$IFDEF UNIX}
  Result := 'Unix ';
  {$ELSE}
  {$IFDEF WINDOWS}
    case Win32MajorVersion of
    0..3:;
    4: begin
     if Win32Platform = VER_PLATFORM_WIN32_NT
     then Result := 'Windows NT v.4 '
     else
       case Win32MinorVersion of
         10: Result := 'Windows 98 ';
         90: Result := 'Windows ME ';
       else
         Result :='Windows 95 ';
       end;
    end;
    5: begin
     case Win32MinorVersion of
       0: Result := 'Windows 2000 ';
       1: Result := 'Windows XP ';
     else
       // XP64 has also a 5.2 version
       // we could detect that based on arch and versioninfo.Producttype
       Result := 'Windows Server2003 ';
     end;
    end;
    6: begin
     case Win32MinorVersion of
       0: Result := 'Windows Vista ';
       1: Result := 'Windows 7 ';
       2: Result := 'Windows 8 ';
     else
       Result := 'Windows 8.1 ';
     end;
    end;
    10: begin
        case Win32MinorVersion of
           0: if Win32BuildNumber >= 22000 then
                Result := 'Windows 11 '
              else
                Result := 'Windows 10 '
        end; {case}
      end
    else
      Result := 'Windows ';
    end;
  {$ENDIF}
  {$ENDIF}
  {$ENDIF}
  {$ENDIF}
  NoOp;
end;

procedure TForm1.PackThisTable(var aDB: TDbf);
begin
  begin
    aDB.Exclusive := True;
    aDB.Active := True;
    if aDB.RecordCount > 0 then
      aDB.PackTable;
    aDB.Exclusive := True;
    aDB.Active := True;
    UpdateStatusBar;
    aDB.First;
  end;
end;

procedure TForm1.PageControlAboutChange(Sender: TObject);
begin
  with PageControlAbout do
    if (ActivePageIndex = TabSheetInfo.PageIndex) and Showing and
      Visible and Enabled then
      UpdateStatusBar;
end;

procedure TForm1.PageControlMainChange(Sender: TObject);
var
  i: Integer = 0;
begin
  for i := 0 to Form1.ComponentCount - 1 do
    with Form1 do
      if Components[i] is TDBGrid then
      begin
        if TCustomGrid(Components[i]).Showing then
          AdjustColumns(Components[i]);
        with TDBGrid(Components[i]) do
          Options := Options + [dgRowSelect];
      end;
  if DBGridList.Showing then
    DBGridListSetColumnWidths;
  UpdateStatusBar;
end;

procedure TForm1.PageControlMainResize(Sender: TObject);
begin
  UpdateStatusBar; //ensures correct DBF for current tab is shown on statusbar
end;

procedure TForm1.PageControlSourceCodeChange(Sender: TObject);
begin
  SetTabSheetMemoButtons(Sender);
end;

procedure TForm1.ParseFilesList;
begin
  with D[iList].aDB do
  begin
    First;
    while not EOF do
    begin
      ParseTitleNEpNum(FieldByName('FROM').AsString);
      GetEpName(FieldByName('FROM').AsString);
      Next;
    end;
    actLookupEpsExecute(Form1);
    AssignRootNameToAssociatedFiles;
    SetListTextWidth;
    IndexName := 'SORTORDER';
    RegenerateIndexes;
  end;
end;

procedure TForm1.ParseHTMLToMemo(Sender: TObject);
var
  Client: TFPHttpClient = nil;
  IsDone: Boolean = False;
  s: String = '';
  sCurTitle: String = '';
  sLeft: String = '';
  sOut: String = '';
  sRight: String = '';

  function FormatEpStr(s: String): String;
  var
    Delims: TSysCharSet = ['-', 'x', 'X', 'e', 'E'];
  begin
    try
      Result := 'S' + Format('%.*d', [2, StrToInt(ExtractDelimited(1, s, Delims))]) +
        'E' + Format('%.*d', [2, StrToInt(ExtractDelimited(2, s, Delims))]);
    except
      Result := '';
    end;
  end;

begin
  Memo1.Clear;
  if Assigned(D[iTempEps].aDB) then
  begin
    ZapPackTable(D[iTempEps].aDB);
    D[iTempEps].aDB.Append;
    sURL := trim(sURL);
    if (sURL <> EmptyStr) and (sURL <> EpGuidesURL) then
    begin
      { SSL initialization has to be done by hand here }
      //InitSSLInterface;
      Client := TFPHttpClient.Create(nil);
      try
        { Allow redirections }
        Client.AllowRedirect := True;
        s := Client.Get(sURL)
      except
        on E: EHttpClient do
          ShowMessage(e.message)
        else
          raise;
      end;
      FreeAndNil(Client);
      if s <> EmptyStr then
      begin
        Memo1.Lines.BeginUpdate;
        //this crazyness is to weed out html pages which don't conform to what we're expecting
        IsDone := (HackFrom('<a href="http://www.imdb.com/title/',
          s, sLeft, sRight) = EmptyStr);
        if not IsDone then
        begin
          s := sRight;
          IsDone := (HackTo('>', s, sLeft, sRight) = EmptyStr);
        end;
        if not IsDone then
        begin
          s := sRight;
          IsDone := (HackTo('</a></h2>', s, sLeft, sRight) = EmptyStr);
        end;
        if not IsDone then
        begin
          s := sRight;
          sOut := sLeft;
          Memo1.Lines.Add(sOut);
          sCurTitle := sOut;
          sOut := '';
        end;
        IsDone := (HackFrom('epinfo left pad''>', s, sLeft, sRight) = EmptyStr);
        while not IsDone do
          with D[iTempEps].aDB do
          begin
            FieldByName('Title').AsString := FixChars(sCurTitle);
            FieldByName('EpNum').AsString :=
              FixChars(FormatEpStr(HackTo('</td>', s, sLeft, sRight)));
            sOut := sOut + ' ' + FieldByName('EpNum').AsString;
            HackFrom('href=', s, sLeft, sRight);
            HackTo('>', s, sLeft, sRight);
            FieldByName('EpName').AsString :=
              FixChars(HackTo('</a>', s, sLeft, sRight));
            sOut := sOut + ' ' + FieldByName('EpName').AsString;
            Memo1.Lines.Add(sOut);
            sOut := '';
            IsDone := (HackFrom('epinfo left pad''>', s, sLeft, sRight) = EmptyStr);
            PostRecord(D[iTempEps].aDB);
            Append;
          end;
        Memo1.Lines.EndUpdate;
      end;
    end;
  end;
  SetTabSheetMemoButtons(Sender);
end;

function TForm1.ParseTitleNEpNum(const curFile: String): String;
var
  sUpExt: String = '';
begin
  Result := '';
  with D[iList].aDB do
  begin
    Edit;
    if ContainsText(FieldByName('From').AsString, ExtractFilePath(curFile)) then
      FieldByName('From').AsString := ExtractFileName(curFile);
    FieldByName('Extension').AsString := ExtractFileExt(curFile);
    AssignListFieldIfEmpty('NameNPath', curFile);
    FieldByName('Title').AsString := EmptyStr;
    AssignListFieldIfEmpty('FromPath', ExtractFilePath(curFile));
    AssignListFieldIfEmpty('ToPath', ExtractFilePath(curFile));
    sUpExt := UpCase(FieldByName('Extension').AsString);
    //assign video file 0, associated file 1, anything else 9
    if Pos(sUpExt, VideoFileExtensions) > 0 then
      FieldByName('TreeLevel').AsString := '0'
    else
      FieldByName('TreeLevel').AsString :=
        IfThen((Pos(sUpExt, '.IDX.SUB.NFO.SRT') > 0), '1', '9');
    if pos(FieldByName('TreeLevel').AsString, '01') > 0 then //video or associated file
    begin
      GetMainAttributes(curFile);
      FieldByName('EpNum').AsString := GetEpNum(curFile);
    end;
    PostRecord(D[iList].aDB);
  end;
end;

procedure TForm1.PostRecord(var aDB: TDbf);
var
  TempReadOnly: Boolean = False;
  s: String = '';
begin
  TempReadOnly := aDB.ReadOnly;
  aDB.ReadOnly := False;
  if (aDB.State <> dsInsert) and (aDB.State <> dsEdit) then
    aDB.Edit;
  try
    aDB.TryExclusive; //probably unnecessary, but abundance of caution here
    aDB.Post;
  except
    s := aDB.Name + ShowDataSetStates(aDB) + LF + GetDBFStatus(aDB);
    ShowMessage('Error: Could not post.' + LineEnding +
      'Suggest ZAPPING the databases, then trying again.' + LF + s);
  end;
  aDB.EndExclusive;
  aDB.ReadOnly := TempReadOnly;
end;

function TForm1.ProbeFile(sFind: String): String;
var
  sCommandLine: String = '';
begin
  Result := '';
  if FilePathFFProbe <> EmptyStr then
  begin
    sCommandLine := sWineFFProbe + FilePathFFProbe +
      ' -v quiet -show_format -show_streams "' + sFind + '"';
    if FilePathFFProbe <> EmptyStr then
      Result := RunEx(sCommandLine, True);
  end;
end;

function TForm1.QueryThenRename: Boolean;
begin
  Result := False;
  with D[iList].aDB do
    if not EOF then
    begin
      sFrom := trim(ExtractFileNameOnly(FieldByName('FROM').AsString) +
        ExtractFileExt(FieldByName('FROM').AsString));
      sFromPath := trim(ExtractFilePath(FieldByName('FROM').AsString));
      sTo := trim(ExtractFileName(aPieces[1]) + ExtractFileExt(
        FieldByName('FROM').AsString));
      sToPath := sFromPath; //put new in same dir as old
      if (buttonSelected <> mrAll) then
        buttonSelected :=
          MessageDlgEx('Rename?' + LineEnding + LineEnding +
          'From:' + LineEnding + sFromPath + sFrom + LineEnding +
          'To:' + LineEnding + sToPath + sTo, mtConfirmation,
          [mbYes, mbNo, mbAll], Form1);
      if (buttonSelected = mrYes) or (buttonSelected = mrAll) then
        Result := RenameListItem(D[iList].aDB, sFromPath, sFrom, sToPath, sTo);
      if Result then
      begin
        Edit;
        FieldByName('TO').AsString := sTo;
        FieldByName('TOPATH').AsString := sToPath;
        FieldByName('NAMENPATH').AsString := sToPath + sTo;
        FieldByName('TITLE').AsString := EmptyStr;
        FieldByName('EPNUM').AsString := EmptyStr;
        PostRecord(D[iList].aDB);
      end;
    end;
  if Result then
    SwapFromTo;
end;

function TForm1.RemoveAllSpacesAndDots(s: String): String;
begin
  s := StringReplace(s, '.', '', [rfReplaceAll]);
  Result := StringReplace(s, ' ', '', [rfReplaceAll]);
end;

function TForm1.RemovePunctuation(s: String): String;
var
  i: Integer = 0;
begin
  Result := '';
  s := trim(s);
  if Length(s) > 0 then
    for i := 1 to Length(s) do
      if s[i] in ['A'..'Z', 'a'..'z', '0'..'9'] then
        Result := Result + s[i];
end;

function TForm1.RenameListItem(aDB: TDbf;
  sFromPath, sFrom, sToPath, sTo: String): Boolean;
var
  sCommandLine: String = '';
  sResult: String = '';

  procedure LogIt;
  begin
    with D[iLog].aDB do
    begin
      Append;
      FieldByName('DATETIME').AsString := FormatDateTime('YYYY/MM/DD hh:nn', now);
      FieldByName('FROM').AsString := sFrom;
      FieldByName('FROMPATH').AsString := sFromPath;
      FieldByName('TO').AsString := sTo;
      FieldByName('TOPATH').AsString := sToPath;
      FieldByName('UNDO').AsBoolean := False;
      PostRecord(D[iLog].aDB);
    end;
  end;

  procedure UnLogIt;
  begin
    with D[iLog].aDB do
    begin
      FieldByName('UnDo').AsBoolean := False;//because might not Pack right away
      Delete;
    end;
  end;

begin
  Result := False;
  if (sFromPath + sFrom = EmptyStr) or (sToPath + sTo = EmptyStr) then
    Inc(SkipCount)
  else
    Result := RenameFile(sFromPath + PathDelim + sFrom, sToPath + PathDelim + sTo);
  if UpCase(ExtractFileExt(sFrom)) = '.MKV' then
    if FilePathMKVPropEdit <> EmptyStr then
    begin
      sCommandLine := sWineMKVPropEdit + FilePathMKVPropEdit + ' ' +
        (QuoteChr + sToPath + STo + QuoteChr) + ' -e info -s ' +
        (QuoteChr + 'title=' + sTo + QuoteChr);
      Clipboard.AsText := sCommandLine;
      sResult := RunEx(sCommandLine, True);
    end;
  if Result = True then
  begin
    Inc(CopiedCount);
    case UpCase(aDB.TableName) of
      'LIST.DBF': LogIt;
      'LOG.DBF': UnLogIt;
      else
        NoOp
    end;
  end;
  if Result = False then
  begin
    Inc(SkipCount);
    if MessageDlgEx('Failed to rename -- Path is probably TOO LONG.' +
      LF + 'From: ' + sFromPath + sFrom + LineEnding + 'To: ' + sToPath +
      sTo, mtError, [mbCancel, mbOK], Form1) = mrCancel then IsCancel := True;
  end;
end;

procedure TForm1.RenameThisList(aDB: TDbf);
begin
  CopiedCount := 0;
  SkipCount := 0;
  aDB.First;
  sCurDB := UpCase(aDB.TableName);
  IsCancel := False;
  while (not aDB.EOF) and (not IsCancel) do
  begin
    sFrom := trim(aDB.FieldByName('FROM').AsString);
    sFromPath := trim(aDB.FieldByName('FROMPATH').AsString);
    sTo := trim(aDB.FieldByName('TO').AsString);
    sToPath := trim(aDB.FieldByName('TOPATH').AsString);
    //reverse TO/FROM fields
    if (sCurDB = 'LOG.DBF') then
    begin
      sFrom := trim(aDB.FieldByName('TO').AsString);
      sFromPath := trim(aDB.FieldByName('TOPATH').AsString);
      sTo := trim(aDB.FieldByName('FROM').AsString);
      sToPath := trim(aDB.FieldByName('FROMPATH').AsString);
    end;
    //it's already been or shouldn't be re-named
    if (FileExists(sToPath + sTo)) or (sTo = EmptyStr) or (sFrom = EmptyStr) then
      SkipCount := SkipCount + 1
    else
    begin
      if buttonSelected <> mrAll then
        buttonSelected := MessageDlgEx('Rename this episode?' +
          LineEnding + LineEnding + 'From:' + LineEnding + sFrom +
          LineEnding + 'To:' + LineEnding + sTo, mtConfirmation,
          [mbYes, mbNo, mbAll], Form1);
      if buttonSelected = mrCancel then
        break;
      if buttonSelected = mrNo then
        SkipCount := SkipCount + 1;
      if ((buttonSelected = mrYes) or (buttonSelected = mrAll)) then
        RenameListItem(aDB, sFromPath, sFrom, sToPath, sTo);
    end;
    aDB.Next;
  end;
  MessageRenamedCount(buttonSelected);
end;

procedure TForm1.RightMouseFonts(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if Button = mbRight then
    actFormFontsExecute(Sender);
end;

function TForm1.RunEx(sCommandLine: String; bRedirectErr: Boolean = False): String;
const
  READ_BYTES = 2048;
var
  iBytesRead: Longint = 0;
  iNumBytes: Longint = 0;
  oProcess: TProcess = nil;
  oStream: TMemoryStream = nil;
  oStrings: TStringList = nil;
  Status: Integer = 0;
begin
  oStream := TMemoryStream.Create;
  iBytesRead := 0;
  try
    oProcess := TProcess.Create(Application);
    try
      {$warnings off}//yes, I want to use simpler "depreciated" method
      oProcess.CommandLine := sCommandLine;
      {$warnings on}
      if bRedirectErr then
        oProcess.Options := [poNoConsole, poUsePipes, poStderrToOutPut]
      else
        oProcess.Options := [poNoConsole, poUsePipes];
      try
        oProcess.Execute;
        Status := oProcess.ExitStatus;
      finally
      end;
      while oProcess.Running do
      begin
        oStream.SetSize(iBytesRead + READ_BYTES);
        iNumBytes := oProcess.Output.Read((oStream.Memory + iBytesRead)^, READ_BYTES);
        if iNumBytes > 0 then
          Inc(iBytesRead, iNumBytes)
        else
          Sleep(100);
      end;
      repeat
        oStream.SetSize(iBytesRead + READ_BYTES);
        iNumBytes := oProcess.Output.Read((oStream.Memory + iBytesRead)^, READ_BYTES);
        if iNumBytes > 0 then
          Inc(iBytesRead, iNumBytes);
      until iNumBytes <= 0;
      oStream.SetSize(iBytesRead);
      oStrings := TStringList.Create;
      try
        oStrings.LoadFromStream(oStream);
        Result := oStrings.Text;
      finally
        oStrings.Free;
      end;
    finally
      oProcess.Free;
    end;
  finally
    oStream.Free;
  end;
end;

function TForm1.sAddHint(aDB: TDbf; FieldNum: Integer): String;
begin
  with aDB do
    if trim(Fields[FieldNum].AsString) <> EmptyStr then
      Result := Fields[FieldNum].FieldName + '= ' +
        trim(Fields[FieldNum].AsString) + LineEnding
    else
      Result := '';
end;

function TForm1.SafeGo(var aDB: TDbf; n: Longint): Boolean;
begin
  Result := True;
  if (0 < n) and (n <= aDB.PhysicalRecordCount) then
    aDB.PhysicalRecNo := n
  else
    Result := False;
end;


//procedure TForm1.SearchFiles(AFileName, ASearchPathPrefix: string; Recursive: boolean; var List: TStrings);
//  procedure AddRecursiveFiles(const SearchDir, FileMask: string; Recursive: boolean);
//  var
//    Info : TSearchRec;
//  begin
//    if assigned(Installer) then
//      Installer.Log(VlDebug,Format(SDbgSearchingDir,[SearchDir]));
//    if FindFirst(SearchDir+AllFilesMask,faAnyFile and faDirectory,Info)=0 then
//    begin
//      repeat
//          if ((Info.Attr and faDirectory) = faDirectory) and (Info.Name <> '.') and (Info.Name <> '..') and (Recursive) then
//            AddRecursiveFiles(SearchDir + Info.Name + PathDelim, FileMask, Recursive);
//          if ((Info.Attr and faDirectory) <> faDirectory) and IsWild(Info.Name, FileMask, FileNameCaseSensitive) then
//            List.Add(SearchDir + Info.Name);
//      until FindNext(Info)<>0;
//    end;
//    sysutils.FindClose(Info);
//  end;
//var
//  CurrDir,
//  BasePath: string;
//  i: integer;
//begin
//  if IsRelativePath(AFileName) and (ASearchPathPrefix<>'') then
//    AFileName := IncludeTrailingPathDelimiter(ASearchPathPrefix) + AFileName;
//  BasePath := ExtractFilePath(ExpandFileName(AFileName));
//  AddRecursiveFiles(BasePath, ExtractFileName(AFileName), Recursive);
//  CurrDir:=GetCurrentDir;
//  for i := 0 to Pred(List.Count) do
//    List[i] := ExtractRelativepath(IncludeTrailingPathDelimiter(IncludeTrailingPathDelimiter(CurrDir)+ASearchPathPrefix), List[i]);
//end;

procedure TForm1.SelectAShow;
begin
  if not IsHideSelectAShow then
  begin
    PanelSelect.Visible := True;
    PageControlMain.ActivePageIndex := TabSheetShows.PageIndex;
    AdjustColumns(DBGridShows);
    TabSheetAlignButtons;
    ShowMessage('Did not match Series Name' + LF + 'based upon Title or Episode Names.' +
      LF + LF + 'Select series, or Cancel.', IsHideSelectAShow);
    IsModalWait := True;
    PanelSelect.Cursor := crDefault;
    BitBtnSelect.Cursor := crDefault;
    BitBtnCancel.Cursor := crDefault;
    StatusBar.Panels[0].Text := 'SELECT A SHOW';
    ShowsFilterEdit.Text := D[iShows].aFilterText;
    if ShowsFilterEdit.Showing then
      ShowsFilterEdit.SetFocus;
    D[iShows].aDB.Filtered := True;
    repeat
      Application.ProcessMessages;
    until (IsModalWait = False) or IsExit;
  end;
end;

procedure TForm1.SetAllCursors(const CursorConstant: Integer);
var
  k: Integer = 0;
begin
  for k := 0 to Form1.ComponentCount - 1 do
    if IsPublishedProp(Form1.Components[k], 'Cursor') then
      SetPropValue(Form1.Components[k], 'Cursor', CursorConstant);
  Application.ProcessMessages;
end;

procedure TForm1.SetBitBtnUndoEnabled;
begin
  with D[iLog].aDB do
  begin
    DisableControls;
    OnFilterRecord := @FilterLogOnUndo;
    Filtered := True;
    CheckLogCheckBoxes;
    Filtered := False;
    EnableControls;
  end;
end;

procedure TForm1.SetBounds(aControl: TControl; ALeft, ATop, AWidth, AHeight: Integer);
begin
  with aControl do
  begin
    try
      if (Left <> ALeft) or (Top <> ATop) or (Width <> AWidth) or
        (Height <> AHeight) then
        InvalidatePreferredSize;
    finally
      inherited SetBounds(ALeft, ATop, AWidth, AHeight);
    end;
  end;
end;

procedure TForm1.SetFilterEpsOnTitle;
begin
  with D[iEps] do
  begin
    aFilterText := D[iShows].aDB.FieldByName('Title').AsString;
    aFilterDescription := QuoteChr + aFilterText + QuoteChr;
  end;
  with D[iEps].aDB do
  begin
    OnFilterRecord := @FilterEpsOnTitle;
    Filtered := True;
    FindFirst;
    EnableControls;
  end;
end;

procedure TForm1.SetListTextWidth;
var
  bmp: TBitmap = nil;
  curRec: Integer = 0;
  curWidth: Integer = 0;
  Widest: Integer = 0;
begin
  if not IsFormSnapped then
    with D[iList].aDB do
      if RecordCount > 0 then
      begin
        curRec := RecNo;
        bmp := TBitMap.Create;
        curWidth := 0;
        Widest := 0;
        DisableControls;
        First;
        while not EOF do
        begin
          curWidth := bmp.Canvas.TextWidth(trim(FieldByName('From').AsString));
          if Widest < curWidth then
            Widest := curWidth;
          Next;
        end;
        Widest := Widest + 60;
        if Widest * 2 > Form1.Width then
          Form1.Width := Widest * 2;
        DBGridList.Width := Widest;
        RecNo := curRec;
        EnableControls;
        FreeAndNil(bmp);
      end;
end;

procedure TForm1.SetRefreshButton(Navigator: TDBNavigator);
begin
  if (Navigator.DataSource <> nil) then
    if (Navigator.DataSource.DataSet.Active) then
      //to control buttons, we must set disable Option navFocusableButtons
      with THackDBNavigator(Navigator) do
      begin
        Options := Options - [navFocusableButtons];
        Buttons[nbRefresh].Enabled := Navigator.DataSource.DataSet.RecordCount > 0;
        Buttons[nbRefresh].Hint := 'Zap ' + TDbf(Navigator.DataSource.DataSet).TableName;
      end;
end;

procedure TForm1.SetTabSheetListButtons;
var
  CountFrom: Integer = 0;
  CountTo: Integer = 0;
begin
  with D[iList].aDB do
  begin
    DisableControls;
    BookMark := GetBookMark;
    BitBtnClear.Enabled := (PhysicalRecordCount > 0);
    First;
    while not EOF do
    begin
      if FieldByName('From').AsString <> EmptyStr then
        Inc(CountFrom);
      if FieldByName('To').AsString <> EmptyStr then
        Inc(CountTo);
      Next;
    end;
    BitBtnLookupEps.Enabled := (CountFrom > 0) and (CountTo < CountFrom);
    BitBtnRename.Enabled := (CountTo > 0);
    GotoBookMark(BookMark);
    EnableControls;
  end;
end;

procedure TForm1.SetTabSheetMemoButtons(Sender: TObject);
var
  s: String = '';
  p: Integer = 0;
  b: Boolean = False;
begin
  p := PageControlSourceCode.ActivePageIndex;
  s := 'EditButton' + p.ToString;
  MyComponent := PageControlSourceCode.Page[p].FindComponent(s);
  b := (TEditButton(MyComponent).Text <> EmptyStr);
  TEditButton(MyComponent).Button.Enabled := b;
  s := 'BitBtnNext' + p.ToString;
  MyComponent := PageControlSourceCode.Page[p].FindComponent(s);
  TBitBtn(MyComponent).Enabled := b;
  s := 'BitBtnPrev' + p.ToString;
  MyComponent := PageControlSourceCode.Page[p].FindComponent(s);
  TBitBtn(MyComponent).Enabled := b;
end;

procedure TForm1.SetTabSheetSourceCodeButtons(Sender: TObject);
begin
  BitBtnCopyEps.Enabled := (Memo1.Lines.Count > 0);
end;

procedure TForm1.SetupDbAbout;
var
  FileVerInfo: TFileVersionInfo = nil;
  fs: TStringStream = nil;
  pHTML: TIpHtml = nil;
  s: String = '';

  procedure AddRow(s1, s2: String);
  begin
    with D[iAbout].aDB do
    begin
      Append;
      FieldByName('Info').AsString := s1;
      FieldByName('Value').AsString := s2;
      PostRecord(D[iAbout].aDB);
    end;
  end;

begin
  s := '<div>Attribution:</div>';
  s := s + '<div>TV Icon made by <a href="https://www.flaticon.com/authors/photo3idea-studio" title="photo3idea_studio">photo3idea_studio</a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a></div>';
  s := s + '<div><a href="https://www.flaticon.com/free-icon/television_926087">https://www.flaticon.com/free-icon/television_926087</a>';
  fs := TStringStream.Create(s);
  try
    pHTML := TIpHtml.Create;// Beware: Will be freed automatically by IpHtmlPanelBrowser
    pHTML.LoadFromStream(fs)
  finally
    fs.Free;
  end;
  IpHtmlPanelAttribution.SetHtml(pHTML);
  begin
    AboutDBGrid.DataSource := D[iAbout].aDataSource;
    ZapPackTable(D[iAbout].aDB);
    s := ExtractFileName(Application.ExeName);
    AddRow('FileName:', s);
    FileVerInfo := TFileVersionInfo.Create(Form1);
    try
      FileVerInfo.ReadFileInfo;
      AddRow('File version:', FileVerInfo.VersionStrings.Values['FileVersion']);
      AddRow('File description:', FileVerInfo.VersionStrings.Values['FileDescription']);
      AddRow('CompanyName:', FileVerInfo.VersionStrings.Values['CompanyName']);
    finally
      NoOp;
    end;
    AddRow('', '');
    AddRow('OSVersion', OSVersion
{$ifdef Windows} + Win32CSDVersion {$endif}
      );
    AddRow('IsLinux', IfThen(IsLinux));
    AddRow('IsWindows', IfThen(IsWindows));
    AddRow('IsWine', IfThen(IsWine, 'True, Wine OS = ' + GetEnv('OS'), 'False'));
    {AddRow('DefaultSystemCodePage', IntToStr(DefaultSystemCodePage) +
      IfThen(DefaultSystemcodePage = 65001, ' (UTF-8)', ''));}
    AddRow('IsInternetConnected', IfThen(IsInternetConnected));
    AddRow('ProgramPath', ProgramPath);
    AddRow('OpPath', OpPath);
    AddRow('ConfigFileDir', ConfigFileDir);
    AddRow('IniFile', IniFile.FileName);
    if Assigned(D[iShows].aDB) then
      with D[iShows].aDB do
        AddRow(TableName, FilePathFull);
    if Assigned(D[iEps].aDB) then
      with D[iEps].aDB do
        AddRow(TableName, FilePathFull);
    if Assigned(D[iTempEps].aDB) then
      with D[iTempEps].aDB do
        AddRow(TableName, 'RecordCount/PhysicalRecordCount = ' +
          IntToStr(RecordCount) + '/' + IntToStr(PhysicalRecordCount));
    AddRow(FileNameFFProbe + IfThen(FileExists(FilePathFFProbe),
      ' [exists]', ' NOT FOUND'), {sWineFFProbe +} FilePathFFProbe);
    AddRow(FileNameMKVPropEdit + IfThen(FileExists(FilePathMKVPropEdit),
      ' [exists]', ' NOT FOUND'), {sWineMKVPropEdit +} FilePathMKVPropEdit);
    AddRow('allshows.txt' + IfThen(FileExists(OpPath + AllShowsTxt),
      ' [exists]', ' NOT FOUND'), OpPath + AllShowsTxt);
    if IsWindows then
    begin
      AddRow('libeay32.dll' + IfThen(FileExists(FileNamelibeay),
        ' [exists]', ' NOT FOUND'), FilePathlibeay);
      AddRow('ssleay32.dll' + IfThen(FileExists(FileNamessleay),
        ' [exists]', ' NOT FOUND'), FilePathssleay);
    end;
    AddRow('PATH', GetEnvironmentVariable('PATH'));
  end;
  with AboutDBGrid do
  begin
    Options := Options + [dgRowSelect] + [dgCellHints];
    Columns[0].SizePriority := 0;
    Columns[1].SizePriority := 0;
    AutoAdvance := aaDown;
    AutoFillColumns := True;
    AdjustColumns(AboutDBGrid);
  end;
  D[iAbout].aDB.EnableControls;
  D[iAbout].aDB.First;
  D[iAbout].aTabIndex := TabSheetAbout.TabIndex;
  FreeAndNil(FileVerInfo);
end;

procedure TForm1.SetupDbEps;
var
  i: Integer = 0;
begin
  with DBGridEps, D[iEps].aDB do
  begin
    Exclusive := True;
    Open;
    EnableControls;
    Active := True;  //active, to get physicalrecordcount
    DBGridEps.DataSource := D[iEps].aDataSource;
    Columns.Clear;
    if Fields.Count > 0 then
      for i := 0 to Fields.Count - 1 do
      begin
        Columns.Add;
        Columns[i].Field := Fields[i];
        Columns[i].Title.Caption :=
          AnsiProperCase(Fields[i].FieldName, [' ', '_']);
        Columns[i].ButtonStyle := cbsAuto;
        Columns[i].SizePriority := IfThen(i < 2, 0, 1);
        if Fields[i].FieldName = 'SORTORDER' then
          Columns[i].Visible := False; //don't show SORTORDER field
      end;
    Options := Options + [dgRowSelect] + [dgCellHints];
    Enabled := True;
    Visible := True;
    IndexName := EmptyStr;
    IndexDefs.Update;
    if (IndexDefs.Count = 1) and (IndexDefs[0].Name = 'SORTORDER') then
      DeleteIndex('SORTORDER');
    //ixExpression seems to cause MEMORY LEAK, so use "SortOrder" field instead
    if IndexDefs.Count = 0 then
      D[iEps].aDB.AddIndex('SORTORDER', 'SORTORDER', [ixCaseInsensitive, ixUnique]);
    D[iEps].aDB.IndexName := 'SORTORDER';
    //RegenerateIndexes;
    DBGridEps.AutoFillColumns := True;
    PopUpMenu := MenuEps;
    D[iEps].aTabIndex := TabSheetEps.TabIndex;
    DBGridEps.Options := DBGridEps.Options - [dgRowSelect] + [dgCellHints];
    DBGridEps.Enabled := True;
    DBGridEps.ReadOnly := False;
    DBGridEps.ShowHint := True;
    AutoAdvance := aaDown;
    DBNavigatorEps.DataSource := D[iEps].aDataSource;
    if Assigned(D[iTempEps].aDB) then
      D[iTempEps].aTabIndex := -1;
  end;
  D[iEps].aDB.First;
end;

procedure TForm1.SetupDbList;
//FROMPATH and TOPATH usually will be the same, both here for future flexibility
begin
  with DBGridList, D[iList].aDB do
  begin
    AfterScroll := @DBGridListAfterScroll;   // must be inactive to set this
    BeforeScroll := @DBGridListBeforeScroll;
    Exclusive := True;
    Open;
    EnableControls;
    Active := True;  //active, to get physicalrecordcount
    DBGridList.DataSource := D[iList].aDataSource;
    Columns.Clear;
    Columns.Add;
    Columns[0].Field := FieldByName('FROM');
    Columns[0].Title.Caption :=
      AnsiProperCase(FieldByName('FROM').FieldName, [' ', '_']);
    Columns[0].ButtonStyle := cbsAuto;
    Columns[0].Width := (DBGridList.Width div 2) - DEFINDICATORCOLWIDTH;
    Columns.Add;
    Columns[1].Field := FieldByName('TO');
    Columns[1].Title.Caption := AnsiProperCase(FieldByName('TO').FieldName, [' ', '_']);
    Columns[1].ButtonStyle := cbsAuto;
    Columns[1].Width := Columns[0].Width;
    Options := Options + [dgRowSelect] + [dgCellHints];
    Enabled := True;
    Visible := True;
    AutoAdvance := aaDown;
  end;
  PanelFilesGrid.ShowHint := True;
  PanelFilesGrid.Hint := 'Drop here files to be renamed';
  actClearExecute(D[iList].aDB);
  D[iList].aDB.First;
  DBGridListSetColumnWidths;
  D[iList].aTabIndex := TabSheetList.TabIndex;
end;

procedure TForm1.SetupDbLog;
begin
  with D[iLog].aDB do
  begin
    AfterPost := @LogAfterPost;
    AfterScroll := @LogAfterScroll;   // must be inactive to set this
    BeforeScroll := @LogBeforeScroll;
    BeforeEdit := @LogBeforeEdit;
    EnableControls;
    Active := True;  //active, to get physicalrecordcount
    DBNavigatorLog.DataSource := D[iLog].aDataSource;
  end;
  with DBGridLog, D[iLog].aDB do
  begin
    DBGridLog.DataSource := D[iLog].aDataSource;
    Columns.Clear;
    Columns.Add;
    Columns[0].Field := FieldByName('UNDO');
    Columns[0].Title.Caption :=
      AnsiProperCase(FieldByName('UNDO').FieldName, [' ', '_']);
    Columns[0].ButtonStyle := cbsAuto;
    Columns[0].MinSize := 20;
    Columns[0].SizePriority := 0;
    Columns.Add;
    Columns[1].Field := FieldByName('DATETIME');
    Columns[1].Title.Caption :=
      AnsiProperCase(FieldByName('DATETIME').FieldName, [' ', '_']);
    Columns[1].ButtonStyle := cbsAuto;
    Columns[1].MinSize := 100;
    Columns[1].SizePriority := 0;
    Columns.Add;
    Columns[2].Field := FieldByName('FROM');
    Columns[2].Title.Caption :=
      AnsiProperCase(FieldByName('FROM').FieldName, [' ', '_']);
    Columns[2].ButtonStyle := cbsAuto;
    Columns[2].Width :=
      ((Width - Columns[0].Width - Columns[1].Width) div 2) - DEFINDICATORCOLWIDTH;
    Columns[2].SizePriority := 0;
    Columns.Add;
    Columns[3].Field := FieldByName('TO');
    Columns[3].Title.Caption := AnsiProperCase(FieldByName('TO').FieldName, [' ', '_']);
    Columns[3].ButtonStyle := cbsAuto;
    Columns[3].Width := Columns[2].Width;
    Options := Options - [dgRowSelect] + [dgCellHints] +
      [dgAlwaysShowEditor] + [dgDisableInsert];
    Columns[3].SizePriority := 0;
    Enabled := True;
    Visible := True;
    Top := 2;
    Left := 2;
    ShowHint := True;
    AutoAdvance := aaDown;
    AutoFillColumns := True;
    AdjustColumns(DBGridLog);
    D[iLog].aTabIndex := TabSheetLog.TabIndex;
  end;
end;

procedure TForm1.SetupDbShows;
var
  i: Integer = 0;
begin
  if Assigned(D[iShows].aDB) then
  begin
    with DBGridShows, D[iShows].aDB do
    begin
      AfterScroll := @DBGridShowsAfterScroll;   // must be inactive to set this
      BeforeScroll := @DBGridShowsBeforeScroll;
      D[iShows].aDB.DeleteIndex('TITLE');
      IndexDefs.Update;
      if IndexDefs.Count = 0 then
        D[iShows].aDB.AddIndex('TITLE', 'TITLE', [ixCaseInsensitive, ixUnique]);
      D[iShows].aDB.IndexName := 'TITLE';
      //D[iShows].aDB.Indexes[0].Name:='TITLE';
      RegenerateIndexes;
      First;
      DBGridShows.DataSource := D[iShows].aDataSource;
      Columns.Clear;
      if (Fields.Count > 0) then
        for i := 0 to (Fields.Count - 1) do
        begin
          Columns.Add;
          Columns[i].Field := Fields[i];
          Columns[I].Title.Caption :=
            AnsiProperCase(Fields[i].FieldName, [' ', '_']);
          Columns[i].ButtonStyle := cbsAuto;
          Columns[i].MinSize := Max(0, Columns[i].Tag);
        end;
      if (Columns.Count >= 2) then
      begin
        Columns[0].MinSize := 140;
        Columns[0].SizePriority := 0;
        Columns[1].MinSize := 140;
        Columns[1].SizePriority := 0;
      end;
      DBGridShows.Enabled := True;
      DBGridShows.AutoAdjustColumns;
      with DBGridShows.Columns do
        Move(ColumnByFieldname('LookUpDate').Index, 2);
      Columns[2].MinSize := 140;
      Columns[2].SizePriority := 1;
      for i := 3 to (Fields.Count - 1) do
        Columns[i].Visible := False;
      ShowHint := True;
      AutoAdvance := aaDown;
      AutoFillColumns := True;
      Enabled := True;
      AdjustColumns(DBGridShows);
      Options := Options + [dgRowSelect] + [dgCellHints];
      PopUpMenu := MenuShows;
      ReadOnly := False;
    end;
    DBNavigatorShows.DataSource := D[iShows].aDataSource;
    D[iShows].aTabIndex := TabSheetShows.TabIndex;
    DownloadShowsIfEmpty;
  end;
end;

function TForm1.sField(sFieldName: String): String;
var
  sDot: String = '.';
begin
  with D[iList].aDB do
  begin
    if sFieldName = 'Height' then
      sDot := 'p.'
    else
    if sFieldName = 'Extension' then
      sDot := '';

    Result := FieldByName(sFieldName).AsString +
      IfThen(length(trim(FieldByName(sFieldName).AsString)) = 0, '', sDot);
  end;
end;

function TForm1.ShowDataSetStates(DataSet: TDataSet): String;
  //sanity check to ShowMessage DBF's current condition
var
  s: String = '';
begin
  s := DataSet.Name + LF;
  case DataSet.State of
    dsInactive: s := s + 'dsInactive';
    dsBrowse: s := s + 'dsBrowse';
    dsEdit: s := s + 'dsEdit';
    dsInsert: s := s + 'dsInsert';
    dsSetKey: s := s + 'dsSetKey';
    dsCalcFields: s := s + 'dsCalcFields';
    dsFilter: s := s + 'dsFilter';
    dsNewValue: s := s + 'dsNewValue';
    dsOldValue: s := s + 'dsOldValue';
    dsCurValue: s := s + 'dsCurValue';
    dsBlockRead: s := s + 'dsBlockRead';
    dsInternalCalc: s := s + 'dsInternalCalc';
    dsOpening: s := s + 'dsOpening';
    dsRefreshFields: s := s + 'dsRefreshFields';
  end;
  ShowMessage(s);
  Result := s;
end;

procedure TForm1.ShowDBFsStatusClick(Sender: TObject);
var
  DForm: TDialogForm = nil;
  i: Integer = 0;
  Widest: Integer = 0;
begin
  DForm := TDialogForm.Create(Form1);
  with DForm do
  begin
    DialogMemo.Font := Form1.Canvas.Font;
    Position := poMainFormCenter;
    for i := Ord(Low(D)) to Ord(High(D)) do
    begin
      DialogMemo.Lines.Add(GetDBFStatus(D[i].aDB));
      DialogMemo.Lines.Add(EmptyStr);
    end;
    for i := 0 to DialogMemo.Lines.Count do
      Widest := Max(Widest, Canvas.TextWidth(DialogMemo.Lines[i]));
    DForm.Width := Widest + 200;
    DForm.Height := Form1.Height - 170;
    DForm.Position := poMainFormCenter;
    DForm.Caption := 'Status of DBF files:';
    DForm.ShowModal;
    FreeAndNil(DForm);
  end;
end;

procedure TForm1.ShowDBFStatus(aDB: TDbf);
begin
  ShowMessage(GetDBFStatus(aDB));
end;

procedure TForm1.ShowMessageList(var aList: TStringList);
var
  i: Integer = 0;
  s: String = '';
begin
  for i := 0 to aList.Count - 1 do
    s := s + aList[i] + LineEnding;
  ShowMessage(s);
end;

function ShowMessage(aMessage: String; bIsHidden: Boolean): Boolean; overload;
begin
  Result := False;
  if not bIsHidden then
  begin
    with TTaskDialog.Create(Form1) do
      try
        Caption := 'Information';
        Title := aMessage;
        Text := '';
        CommonButtons := [tcbOK];
        MainIcon := tdiInformation;
        //tdiWarning, tidError, tdiShield, tdiNone, tdiQuestion;
        VerificationText := 'Don''t show this message again this session';
        if Execute then
          Result := tfVerificationFlagChecked in Flags;
      finally
        Free;
      end;
  end;
end;

procedure TForm1.ShowsFilterEditButtonClick(Sender: TObject);
begin
  actFilterEditCancelExecute(Sender);
end;

procedure TForm1.ShowsFilterEditChange(Sender: TObject);
begin
  FindMyTitle(ShowsFilterEdit.Text);
  ShowsFilterEdit.Button.Enabled := ShowsFilterEdit.Text <> EmptyStr;
end;

procedure TForm1.ShowsFilterEditEditingDone(Sender: TObject);
begin
  FindMyTitle(ShowsFilterEdit.Text);
end;

procedure TForm1.ShowsFilterEditKeyPress(Sender: TObject; var Key: Char);
begin
  LastKeyTyped := Key;
end;

procedure TForm1.ShowsFilterEditKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  AdjustColumns(DBGridShows);
end;

procedure TForm1.ShowStatusDBF(var aDB: TDbf; aFilterDescrip: String);
var
  IsUnique: Boolean = False;
begin
  if (Assigned(aDB)) and (aDB.Active) then
  begin
    StatusBar.Visible := True;
    StatusBar.Panels[0].Text := '';
    if (aDB.Indexes.Count > 0) then
      IsUnique := (ixUnique in aDB.Indexes[0].Options) and (aDB.IndexName <> EmptyStr);
    StatusBar.Panels[0].Text :=
      aDB.TableName + ' RecNo:' +
      Format('%5s', [IntToStr(IfThen(aDB.RecordCount > 0, aDB.PhysicalRecNo, 0))]) +
      '/' + IntToStr(aDB.PhysicalRecordCount) + IfThen(
      (aDB.CanModify = True), '', ' READ-ONLY') +
      IfThen(aDB.IndexName <> '', ' Sort:' + aDB.IndexName, '') +
      IfThen(aFilterDescrip <> EmptyStr, ' Filter:' + aFilterDescrip, '') +
      IfThen(IsUnique, ' Unique', ' ') + 'Recs:' + IntToStr(aDB.ExactRecordCount);
  end;
end;

procedure TForm1.StatusBarDrawPanel(statBar: TStatusBar; Panel: TStatusPanel;
  const Rect: TRect);
begin
  if (Panel = statBar.Panels[0]) then
    with statBar.Canvas do
    begin
      FillRect(Rect);
      TextOut(2, 2, Panel.Text);
      StatusBar.Height := StatusBar.Canvas.TextHeight(Panel.Text) + 2;
    end;
end;

procedure TForm1.StatusBarMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if Button = mbRight then
    actStatusBarFontExecute(Sender);
end;

function TForm1.StripAlpha(s: String): String;
var
  i: Integer = 0;
begin
  Result := '';
  s := trim(s);
  if Length(s) > 0 then
    for i := 1 to Length(s) do
      if s[i] in ['0'..'9'] then
        Result := Result + s[i];
end;

function TForm1.StripBadChars(s: String): String;
var
  GoodChars: set of Char = ['0'..'9', 'A'..'Z', 'a'..'z', '-', '_'];
  i: Integer = 0;
  sOut: String = '';
begin
  s := ReplaceStr(s, '.20', '_20');  // eg., ".2012"
  s := ReplaceStr(s, '.19', '_19');  // eg., ".1999"
  s := ReplaceStr(s, '(20', '_20');  // eg., "(2012"
  s := ReplaceStr(s, '(19', '_19');  // eg., "(1999"
  s := ReplaceStr(s, '(', '_');    // eg., "(2020)"
  s := ReplaceStr(s, '.', '');    // get rid of periods
  if length(s) > 0 then
    for i := 1 to length(s) do
      sOut := sOut + IfThen((s[i] in GoodChars), s[i], '');
  ShowsFilterEdit.Hint := sOut + ', Last Key Typed=' + IntToStr(Ord(LastKeyTyped));
  Result := sOut;
end;

function TForm1.StripNonAlphaNum(s: String): String;
var
  i: Integer = 0;
begin
  Result := '';
  s := trim(s);
  if Length(s) > 0 then
    for i := 1 to Length(s) do
      if s[i] in ['0'..'9', 'A'..'Z', 'a'..'z'] then
        Result := Result + s[i];
end;

procedure TForm1.SwapFromTo;
var
  j: Integer = 0;
begin
  with D[iList].aDB do
  begin
    sFrom := FieldByName('TO').AsString;
    if sToPath <> EmptyStr then
      sFromPath := sToPath;
    Edit;
    for j := 0 to FieldCount - 1 do
      Fields[j].AsString := EmptyStr;
    FieldByName('FROM').AsString := sFromPath + sFrom;
    PostRecord(D[iList].aDB);
  end;
end;

function TForm1.sYearAndEpNumOnlyNoEpName: String;
  //deals with files like: partners.2012.103.hdtv.mp4
var
  curFile: String = '';
begin
  Result := '';
  curFile := ExtractFileNameOnly(D[iList].aDB.FieldByName('From').AsString);
  sYear := '';
  sTitle := '';
  sEpName := '';
  FindYear(curFile);
  if sYear <> EmptyStr then
  begin
    sEpNum := GetEpNum(sEpName);
    if sEpNum <> EmptyStr then
      with D[iList].aDB do
      begin
        Edit;
        FieldByName('TITLE').AsString := StripBadChars(sTitle + sYear);
        FieldByName('EPNUM').AsString := sEpNum;
        Post;
        Result := FieldByName('TITLE').AsString;
      end;
  end;
end;

procedure TForm1.SynEditKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
var
  s: String = '';
  cp: Integer = 0;
begin
  cp := PageControlSourceCode.ActivePageIndex;
  s := TObject(Sender).ClassName + cp.ToString;
  s := RightStr(s, length(s) - 1); //chop off the 'T' from component name
  MyComponent := PageControlSourceCode.Page[cp].FindComponent(s);
  case Key of
    VK_RETURN: if SearchDirectionForward then BitBtnNextClick(Sender)
      else
        BitBtnPrevClick(Sender);
    VK_F3: if ssShift in Shift then BitBtnPrevClick(Sender)
      else
        BitBtnNextClick(Sender)
    else
      NoOp
  end; {case}
end;


procedure TForm1.TabSheetAboutShow(Sender: TObject);
begin
  if (PageControlAbout.ActivePageIndex = TabSheetInfo.PageIndex) and
    PageControlAbout.Focused and PageControlAbout.Visible then
    SetupDBAbout;
end;

procedure TForm1.TabSheetAlignButtons;
begin
  if TabSheetShows.Showing then
  begin
    PanelShowsNav.Width := (PanelShowsTop.Width div 2) + 4;
    BitBtnSelect.AutoSize := True;
    BitBtnCancel.AutoSize := True;
    PanelSelect.Height := BitBtnSelect.Height + 20;
    BitBtnSelect.Top := (PanelSelect.Height - BitBtnSelect.Height) div 2;
    BitBtnCancel.Top := BitBtnSelect.Top;
    BitBtnSelect.Left := 5;
    BitBtnCancel.Left := BitBtnSelect.Left + BitBtnSelect.Width + 10;
  end;
end;

procedure TForm1.TabSheetBrowserResize(Sender: TObject);
begin
  btBack.Top := (pnTop.Height - btBack.Height) div 2;
  btForward.Top := (pnTop.Height - btForward.Height) div 2;
  edUrl.Top := (pnTop.Height - edUrl.Height) div 2;
  btGo.Top := (pnTop.Height - btGo.Height) div 2;
end;

procedure TForm1.TabSheetEpsResize(Sender: TObject);
begin
  PanelEpsTop.Width := TabSheetEps.ClientWidth;
  with DBNavigatorEps do
    SetBounds(0, 0, PanelEpsTop.Width - 2, PanelEpsTop.Height - 2);
  DBGridEps.ReAlign;
  SetRefreshButton(DBNavigatorEps);
end;

procedure TForm1.TabSheetEpsShow(Sender: TObject);
begin
  if D[iEps].aDB.FieldByName('Title').AsString <>
    D[iShows].aDB.FieldByName('Title').AsString then SetFilterEpsOnTitle;
  with D[iEps].aDB, D[iEps] do
    if (D[iEps].aDB.Active) and (DBGridEps.Showing) then
    begin
      if PhysicalRecordCount > 0 then
        THackDBNavigator(DBNavigatorEps).Options :=
          THackDBNavigator(DBNavigatorEps).Options + [navFocusableButtons];
      DBGridEps.Options := DBGridEps.Options + [dgRowSelect] -
        [dgEditing] + [dgCellHints];
      if (ExactRecordCount = 0) and (D[iShows].aDB.FieldByName('LookupDate').AsString <>
        sToday) then
        DownloadEps(Sender);
    end;
  DBGridEps.Columns[2].SizePriority := 1;
  DBGridEps.Columns[3].Visible := False;
end;

procedure TForm1.TabSheetInfoResize(Sender: TObject);
begin
  if (PageControlMain.ActivePageIndex = TabSheetAbout.PageIndex) and
    (PageControlAbout.ActivePageIndex = TabSheetInfo.PageIndex) then
    SetupDBAbout;
end;

procedure TForm1.TabSheetInfoShow(Sender: TObject);
begin
  SetupDBAbout;
end;

procedure TForm1.TabSheetListResize(Sender: TObject);
begin
  DBGridListSetColumnWidths;
  PanelTop.Height := BitBtnOpen.Height + 2;
  SetTabSheetListButtons;
end;

procedure TForm1.TabSheetListShow(Sender: TObject);
begin
  SetTabSheetListButtons;
end;

procedure TForm1.TabSheetLogResize(Sender: TObject);
begin
  SetRefreshButton(DBNavigatorLog);
  SetBitBtnUndoEnabled;
  PanelLogTop.Height := BitBtnUndo.Height + 1;
  with BitBtnUndo do
    SetBounds(0, 0, Width, Height);
  with DBNavigatorLog do
    SetBounds(BitBtnUndo.Width + 2, 0, PanelLogTop.Width - BitBtnUndo.Width -
      2, PanelLogTop.ClientHeight);
end;

procedure TForm1.TabSheetLogShow(Sender: TObject);
begin
  if TabSheetLog.Visible and (D[iLog].aDB.RecordCount = 0) then SetupDBLog;
  SetRefreshButton(DBNavigatorLog);
  with D[iLog].aDB do
  begin
    CheckLogCheckBoxes;
    OnFilterRecord := @FilterLogOnUndo;
    Filtered := True;
    FindFirst;
    Filtered := False;
    OnFilterRecord := nil;
    FindFirst;
  end;
end;

procedure TForm1.TabSheetMemoResize(Sender: TObject);
begin
  SetTabSheetMemoButtons(Sender);
end;

procedure TForm1.TabSheetPrivacyShow(Sender: TObject);
begin
  TabSheetShowResource(IpHtmlPanelPrivacy, 'PRIVACY_POLICY.HTML');
end;

procedure TForm1.TabSheetReadmeShow(Sender: TObject);
begin
  TabSheetShowResource(IpHtmlPanelReadme, 'README.html');
end;

procedure TForm1.TabSheetShowResource(aObject: TObject; aFileName: String);
begin
  //don't waste time loading this resource, unless user actually looks at it
  if (aObject is TIpHtmlPanel) and (TIpHtmlPanel(aObject).GetContentSize.Width = 0) then
  begin
    MyObject := aObject;
    SetAllCursors(crHourGlass);
    ExtractResourceToObject(aFileName, aObject);
    SetAllCursors(crDefault);
  end;
end;

procedure TForm1.TabSheetShowsResize(Sender: TObject);
begin
  TabSheetAlignButtons;
end;

procedure TForm1.TabSheetShowsShow(Sender: TObject);
begin
  TabSheetAlignButtons;
  AdjustColumns(DBGridShows);
end;

procedure TForm1.TabSheetSourceCodeShow(Sender: TObject);
begin
  UpdateStatusBar;
end;

//procedure TForm1.TaskDialogTimer(Sender: TObject; TickCount: Cardinal; var Reset: Boolean);
//begin
//   // TaskDialog1.ProgressBar.Position := MyThread.CurrentProgressPercent;
//   // Demo
//   TaskDialog.ProgressBar.Position :=  TaskDialog.ProgressBar.Position + 1;
//end;

procedure TForm1.TestKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  if (ssCtrl in Shift) then
    case Key of
      VK_ADD: IncreaseAllFonts(1);
      VK_SUBTRACT: IncreaseAllFonts(-1);
      else
        NoOp
    end;
end;

procedure TForm1.ToolBarFilesResize(Sender: TObject);
begin
  PanelTop.Constraints.MinHeight :=
    BitBtnExit.Top + BitBtnExit.Height + PanelTop.Top + ToolBarFiles.BorderWidth + 1;
end;

function TForm1.ToolBarWidthSize(aToolBar: TToolbar): Integer;
var
  i: Integer = 0;
  w: Integer = 0;
begin
  if Assigned(aToolBar) then
    if aToolBar.ControlCount > 0 then
    begin
      for i := 0 to aToolBar.ControlCount - 1 do
      begin
        aToolBar.Controls[i].Left := w;
        w := w + aToolBar.Controls[i].Width;
      end;
      w := w + 13;
    end;
  Result := w;
end;

function TForm1.TrimPeriod(s: String): String;
begin
  if (LeftStr(s, 1) = '.') or (LeftStr(s, 1) = '-') then
    s := RightStr(s, length(s) - 1);
  if (RightStr(s, 1) = '.') or (RightStr(s, 1) = '-') then
    s := LeftStr(s, length(s) - 1);
  Result := s;
end;

procedure TForm1.UpdateStatusBar;
begin
  Application.ProcessMessages;
  DisableAlign;
  if (PageControlMain.Showing) and not (PageControlMain.IsResizing) and not
    (Form1.IsResizing) then
  begin
    if PageControlMain.Page[PageControlMain.ActivePageIndex].Showing then
      case PageControlMain.ActivePageIndex of
        0: ShowStatusDBF(D[iList].aDB, '');
        1: ShowStatusDBF(D[iShows].aDB, D[iShows].aFilterDescription);
        2: ShowStatusDBF(D[iEps].aDB, D[iEps].aFilterDescription);
        3: if StatusBar.Visible then StatusBar.Visible := False;
        4: if StatusBar.Visible then StatusBar.Visible := False;
        5: ShowStatusDBF(D[iLog].aDB, D[iLog].aFilterDescription);
        6: if PageControlAbout.ActivePageIndex = TabSheetInfo.PageIndex then
            ShowStatusDBF(D[iAbout].aDB, D[iAbout].aFilterDescription)
          else
          if StatusBar.Visible then StatusBar.Visible := False;
        else
          NoOp
      end;
    if StatusBar.Visible then StatusBar.Hint := StatusBar.Panels[0].Text;
  end;
  EnableAlign;
end;

procedure TForm1.ZapPackTable(var aDB: TDbf);
begin
  with aDB do
    if (PhysicalRecordCount > 0) then //don't bother if nothing to Zap
    begin
      Exclusive := True;
      Active := True;
      ShowDeleted := True;
      //CopyFile(TableName, TableName + '.bak');
      //BackupFileForWrite(TableName);
      DisableControls;
      Zap;
      if RecordCount > 0 then
        PackTable;
      EnableControls;
      ShowDeleted := False;
      Exclusive := True;
      First;
    end;
end;

initialization

  Application.Scaled := True;
  Application.Title := 'filey';  //this name affects GetAppConfigDir

end.
