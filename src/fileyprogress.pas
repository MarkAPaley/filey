unit fileyprogress;

{$mode objfpc}{$H+}

interface

uses
  Buttons, Classes, ComCtrls, Controls, Dialogs, ExtCtrls, Forms,
  Graphics, Math, StdCtrls, SysUtils;

type

  { TProgressForm }

  TProgressForm = class(TForm)
    BitBtn: TBitBtn;
    HeaderPanel: TPanel;
    Label1: TLabel;
    Panel1: TPanel;
    Panel2: TPanel;
    PanelProgress: TPanel;
    ProgressBar1: TProgressBar;
    procedure BitBtnClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var CloseAction: TCloseAction);
    procedure FormResize(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Label1Resize(Sender: TObject);
  public
    IsClicked: Boolean;
  end;

var
  ProgressForm: TProgressForm;

implementation

{$R *.lfm}

procedure TProgressForm.BitBtnClick(Sender: TObject);
begin
  IsClicked := True;
end;

procedure TProgressForm.FormClose(Sender: TObject; var CloseAction: TCloseAction);
begin
  CloseAction := caFree;
end;

procedure TProgressForm.FormResize(Sender: TObject);
begin
  BitBtn.Left := (Panel2.Width - BitBtn.Width) div 2;
  BitBtn.Top := (Panel2.Height - BitBtn.Height) div 2;
  Width := Max(Width, Canvas.TextWidth(Label1.Caption) + 20);
  Label1.Left := Trunc((PanelProgress.Width - Canvas.TextWidth(Label1.Caption)) / 2);
  IsClicked := False;
end;

{ TProgressForm }

procedure TProgressForm.FormShow(Sender: TObject);
begin
  BitBtn.Left := (Panel2.Width - BitBtn.Width) div 2;
  BitBtn.Top := (Panel2.Height - BitBtn.Height) div 2;
  Label1.Left := (PanelProgress.Width - Label1.Width) div 2;
  IsClicked := False;
end;

procedure TProgressForm.Label1Resize(Sender: TObject);
begin
  Width := Max(Width, Canvas.TextWidth(Label1.Caption) + 20);
  Label1.Left := Trunc((PanelProgress.Width - Canvas.TextWidth(Label1.Caption)) / 2);
  BitBtn.Left := (Panel2.Width - BitBtn.Width) div 2;
  BitBtn.Top := (Panel2.Height - BitBtn.Height) div 2;
  Label1.Left := (PanelProgress.Width - Label1.Width) div 2;
  IsClicked := False;
end;

end.
